using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers
{
    using static Expression;

    /// <summary>
    /// A wrapper for EqualityComparers which prevents implementation inlining.
    /// </summary>
    public sealed class NoInlineEqualityComparer<T> : IEqualityComparerEx<T>
    {
        private readonly IEqualityComparer<T> Inner;

        public NoInlineEqualityComparer(IEqualityComparer<T> inner)
        {
            if (inner == null)
                throw new ArgumentNullException(nameof(inner));
            Inner = inner;
        }

        public Boolean Equals(T x, T y) =>
            Inner.Equals(x, y);

        public Int32 GetHashCode(T obj) =>
            Inner.GetHashCode(obj);

        public Expression EqualsExpression(Expression x, Expression y) =>
            Call(Constant(Inner), Reflections.IEqualityComparer<T>.Equals, x, y);

        public Expression GetHashCodeExpression(Expression obj) =>
            Call(Constant(Inner), Reflections.IEqualityComparer<T>.GetHashCode, obj);
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<T> NoInlineEC<T>(IEqualityComparer<T> inner) =>
            new NoInlineEqualityComparer<T>(inner);
    }
}
