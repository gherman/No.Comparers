using System;
using System.Collections.Generic;
using System.Reflection;

namespace No.Comparers
{
    using static EqualityComparerFactories;

    public static class DefaultEqualityComparer<T>
    {
        public static IEqualityComparer<T> Instance { get; }

        static DefaultEqualityComparer()
        {
            var t = typeof(T).GetTypeInfo();
            var iequatablet = typeof(IEquatable<T>).GetTypeInfo();
            Instance = iequatablet.IsAssignableFrom(t) ? EquatableECChecked<T>() : ObjectEC<T>();
            if (!t.IsValueType)
                Instance = NullECChecked(Instance);
        }
    }
}
