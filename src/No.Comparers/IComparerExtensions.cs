using System;
using System.Collections.Generic;

namespace No.Comparers
{
    public static class IComparerExtensions
    {
        public static IEqualityComparer<TKey> ToEqualityComparer<TKey>(this IComparer<TKey> comparer, Func<TKey, Int32> getHashCode)
        {
            return new EqualityComparerFromComparer<TKey>(comparer, getHashCode);
        }
    }
}
