using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace No.Comparers.Expressions
{
    using static EqualityComparerFactories;

    public class ExpressionEqualityComparers
    {
        private sealed class NotSupportedEqualityComparer<T> : InliningEqualityComparer<T>
        {
            private readonly Expression Throw;

            public NotSupportedEqualityComparer(String message)
            {
                Throw = Expression.Throw(
                    Expression.New(
                        Reflections.NotSupportedException.New.FromString,
                        Expression.Constant(message)));
            }

            public override Expression EqualsExpression(Expression x, Expression y) =>
                Expression.Block(Throw, Expression.Default(typeof(Boolean)));

            public override Expression GetHashCodeExpression(Expression obj) =>
                Expression.Block(Throw, Expression.Default(typeof(Int32)));
        }

        protected static IEqualityComparer<Object> IdentityEC { get; } =
            IdentityEC<Object>();

        private static readonly Type[] ExpressionVariants;
        private static readonly Type[] MemberBindingVariants;

        static ExpressionEqualityComparers()
        {
            ExpressionVariants = new Type[Enum.GetValues(typeof(ExpressionType)).Cast<Int32>().Max() + 1];
            ExpressionVariants[(Int32)ExpressionType.Add] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.AddAssign] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.AddAssignChecked] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.AddChecked] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.And] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.AndAlso] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.AndAssign] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.ArrayIndex] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.ArrayLength] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Assign] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Block] = typeof(BlockExpression);
            ExpressionVariants[(Int32)ExpressionType.Call] = typeof(MethodCallExpression);
            ExpressionVariants[(Int32)ExpressionType.Coalesce] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Conditional] = typeof(ConditionalExpression);
            ExpressionVariants[(Int32)ExpressionType.Constant] = typeof(ConstantExpression);
            ExpressionVariants[(Int32)ExpressionType.Convert] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.ConvertChecked] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.DebugInfo] = typeof(DebugInfoExpression);
            ExpressionVariants[(Int32)ExpressionType.Decrement] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Default] = typeof(DefaultExpression);
            ExpressionVariants[(Int32)ExpressionType.Divide] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.DivideAssign] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Dynamic] = typeof(DynamicExpression);
            ExpressionVariants[(Int32)ExpressionType.Equal] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.ExclusiveOr] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.ExclusiveOrAssign] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Extension] = null;
            ExpressionVariants[(Int32)ExpressionType.Goto] = typeof(GotoExpression);
            ExpressionVariants[(Int32)ExpressionType.GreaterThan] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.GreaterThanOrEqual] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Increment] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Index] = typeof(IndexExpression);
            ExpressionVariants[(Int32)ExpressionType.Invoke] = typeof(InvocationExpression);
            ExpressionVariants[(Int32)ExpressionType.IsFalse] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.IsTrue] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Label] = typeof(LabelExpression);
            ExpressionVariants[(Int32)ExpressionType.Lambda] = typeof(LambdaExpression);
            ExpressionVariants[(Int32)ExpressionType.LeftShift] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.LeftShiftAssign] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.LessThan] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.LessThanOrEqual] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.ListInit] = typeof(ListInitExpression);
            ExpressionVariants[(Int32)ExpressionType.Loop] = typeof(LoopExpression);
            ExpressionVariants[(Int32)ExpressionType.MemberAccess] = typeof(MemberExpression);
            ExpressionVariants[(Int32)ExpressionType.MemberInit] = typeof(MemberInitExpression);
            ExpressionVariants[(Int32)ExpressionType.Modulo] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.ModuloAssign] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Multiply] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.MultiplyAssign] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.MultiplyAssignChecked] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.MultiplyChecked] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Negate] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.NegateChecked] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.New] = typeof(NewExpression);
            ExpressionVariants[(Int32)ExpressionType.NewArrayBounds] = typeof(NewArrayExpression);
            ExpressionVariants[(Int32)ExpressionType.NewArrayInit] = typeof(NewArrayExpression);
            ExpressionVariants[(Int32)ExpressionType.Not] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.NotEqual] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.OnesComplement] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Or] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.OrAssign] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.OrElse] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Parameter] = typeof(ParameterExpression);
            ExpressionVariants[(Int32)ExpressionType.PostDecrementAssign] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.PostIncrementAssign] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Power] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.PowerAssign] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.PreDecrementAssign] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.PreIncrementAssign] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Quote] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.RightShift] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.RightShiftAssign] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.RuntimeVariables] = typeof(RuntimeVariablesExpression);
            ExpressionVariants[(Int32)ExpressionType.Subtract] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.SubtractAssign] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.SubtractAssignChecked] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.SubtractChecked] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Switch] = typeof(SwitchExpression);
            ExpressionVariants[(Int32)ExpressionType.Throw] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Try] = typeof(TryExpression);
            ExpressionVariants[(Int32)ExpressionType.TypeAs] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.TypeEqual] = typeof(BinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.TypeIs] = typeof(TypeBinaryExpression);
            ExpressionVariants[(Int32)ExpressionType.UnaryPlus] = typeof(UnaryExpression);
            ExpressionVariants[(Int32)ExpressionType.Unbox] = typeof(UnaryExpression);

            MemberBindingVariants = new Type[Enum.GetValues(typeof(MemberBindingType)).Cast<Int32>().Max() + 1];
            MemberBindingVariants[(Int32)MemberBindingType.Assignment] = typeof(MemberAssignment);
            MemberBindingVariants[(Int32)MemberBindingType.ListBinding] = typeof(MemberListBinding);
            MemberBindingVariants[(Int32)MemberBindingType.MemberBinding] = typeof(MemberMemberBinding);
        }

        public virtual IEqualityComparer<BinaryExpression> BinaryExpressionEC { get; }
        public virtual IEqualityComparer<BlockExpression> BlockExpressionEC { get; }
        public virtual IEqualityComparer<CatchBlock> CatchBlockEC { get; }
        public virtual IEqualityComparer<ConditionalExpression> ConditionalExpressionEC { get; }
        public virtual IEqualityComparer<ConstantExpression> ConstantExpressionEC { get; }
        public virtual IEqualityComparer<DebugInfoExpression> DebugInfoExpressionEC { get; }
        public virtual IEqualityComparer<DefaultExpression> DefaultExpressionEC { get; }
        public virtual IEqualityComparer<DynamicExpression> DynamicExpressionEC { get; }
        public virtual IEqualityComparer<ElementInit> ElementInitEC { get; }
        public virtual IEqualityComparer<Expression> ExpressionEC { get; }
        public virtual IEqualityComparer<GotoExpression> GotoExpressionEC { get; }
        public virtual IEqualityComparer<IndexExpression> IndexExpressionEC { get; }
        public virtual IEqualityComparer<InvocationExpression> InvocationExpressionEC { get; }
        public virtual IEqualityComparer<LabelExpression> LabelExpressionEC { get; }
        public virtual IEqualityComparer<LabelTarget> LabelTargetEC =>
            IdentityEC;
        public virtual IEqualityComparer<LambdaExpression> LambdaExpressionEC { get; }
        public virtual IEqualityComparer<ListInitExpression> ListInitExpressionEC { get; }
        public virtual IEqualityComparer<LoopExpression> LoopExpressionEC { get; }
        public virtual IEqualityComparer<MemberAssignment> MemberAssignmentEC { get; }
        public virtual IEqualityComparer<MemberBinding> MemberBindingEC { get; }
        public virtual IEqualityComparer<MemberExpression> MemberExpressionEC { get; }
        public virtual IEqualityComparer<MemberInitExpression> MemberInitExpressionEC { get; }
        public virtual IEqualityComparer<MemberListBinding> MemberListBindingEC { get; }
        public virtual IEqualityComparer<MemberMemberBinding> MemberMemberBindingEC { get; }
        public virtual IEqualityComparer<MethodCallExpression> MethodCallExpressionEC { get; }
        public virtual IEqualityComparer<NewArrayExpression> NewArrayExpressionEC { get; }
        public virtual IEqualityComparer<NewExpression> NewExpressionEC { get; }
        public virtual IEqualityComparer<ParameterExpression> ParameterExpressionEC =>
            IdentityEC;
        public virtual IEqualityComparer<RuntimeVariablesExpression> RuntimeVariablesExpressionEC { get; }
        public virtual IEqualityComparer<Expression> SubexpressionEC =>
            ExpressionEC;
        public virtual IEqualityComparer<SwitchCase> SwitchCaseEC { get; }
        public virtual IEqualityComparer<SwitchExpression> SwitchExpressionEC { get; }
        public virtual IEqualityComparer<TryExpression> TryExpressionEC { get; }
        public virtual IEqualityComparer<TypeBinaryExpression> TypeBinaryExpressionEC { get; }
        public virtual IEqualityComparer<UnaryExpression> UnaryExpressionEC { get; }

        public ExpressionEqualityComparers()
        {
            BinaryExpressionEC = new ComponentsEqualityComparer<BinaryExpression>
            {
                { _ => _.NodeType, OperatorEC<ExpressionType>() },
                { _ => _.Type, IdentityEC },
                { _ => _.Method, IdentityEC },
                { _ => _.Left, SubexpressionEC },
                { _ => _.Right, SubexpressionEC },
                { _ => _.Conversion, SubexpressionEC },
            };
            BlockExpressionEC = new ComponentsEqualityComparer<BlockExpression>
            {
                { _ => _.Type, IdentityEC },
                { _ => _.Expressions, SequenceEC(SubexpressionEC) },
                { _ => _.Variables, MultiSetEC(ParameterExpressionEC) },
            };
            CatchBlockEC = new ComponentsEqualityComparer<CatchBlock>
            {
                { _ => _.Test, IdentityEC },
                { _ => _.Variable, ParameterExpressionEC },
                { _ => _.Body, SubexpressionEC },
                { _ => _.Filter, SubexpressionEC },
            };
            ConditionalExpressionEC = new ComponentsEqualityComparer<ConditionalExpression>
            {
                { _ => _.Type, IdentityEC },
                { _ => _.Test, SubexpressionEC },
                { _ => _.IfTrue, SubexpressionEC },
                { _ => _.IfFalse, SubexpressionEC },
            };
            ConstantExpressionEC = new ComponentsEqualityComparer<ConstantExpression>
            {
                { _ => _.Type, IdentityEC },
                { _ => _.Value, NullEC(EqualityComparer<Object>.Default) },
            };
            DebugInfoExpressionEC = new NotSupportedEqualityComparer<DebugInfoExpression>("Comparing debug information nodes is not supported.");
            DefaultExpressionEC = new ComponentsEqualityComparer<DefaultExpression>
            {
                { _ => _.Type, IdentityEC },
            };
            DynamicExpressionEC = new ComponentsEqualityComparer<DynamicExpression>
            {
                { _ => _.DelegateType, IdentityEC },
                { _ => _.Binder, IdentityEC },
                { _ => _.Arguments, SequenceEC(SubexpressionEC) },
            };
            ElementInitEC = new ComponentsEqualityComparer<ElementInit>
            {
                { _ => _.AddMethod, IdentityEC },
                { _ => _.Arguments, SequenceEC(SubexpressionEC) },
            };
            ExpressionEC = NullEC(new VariantsEqualityComparer<Expression>(ex => ExpressionVariants[(Int32)ex.NodeType])
            {
                BinaryExpressionEC,
                BlockExpressionEC,
                ConditionalExpressionEC,
                ConstantExpressionEC,
                DebugInfoExpressionEC,
                DefaultExpressionEC,
                DynamicExpressionEC,
                GotoExpressionEC,
                IndexExpressionEC,
                InvocationExpressionEC,
                LabelExpressionEC,
                LambdaExpressionEC,
                ListInitExpressionEC,
                LoopExpressionEC,
                MemberExpressionEC,
                MemberInitExpressionEC,
                MethodCallExpressionEC,
                NewArrayExpressionEC,
                NewExpressionEC,
                ParameterExpressionEC,
                RuntimeVariablesExpressionEC,
                SwitchExpressionEC,
                TryExpressionEC,
                TypeBinaryExpressionEC,
                UnaryExpressionEC,
            });
            GotoExpressionEC = new ComponentsEqualityComparer<GotoExpression>
            {
                { _ => _.Kind, OperatorEC<GotoExpressionKind>() },
                { _ => _.Target, LabelTargetEC },
                { _ => _.Value, SubexpressionEC },
            };
            IndexExpressionEC = new ComponentsEqualityComparer<IndexExpression>
            {
                { _ => _.Indexer, IdentityEC },
                { _ => _.Object, SubexpressionEC },
                { _ => _.Arguments, SequenceEC(SubexpressionEC) },
            };
            InvocationExpressionEC = new ComponentsEqualityComparer<InvocationExpression>
            {
                { _ => _.Expression, SubexpressionEC },
                { _ => _.Arguments, SequenceEC(SubexpressionEC) },
            };
            LabelExpressionEC = new ComponentsEqualityComparer<LabelExpression>
            {
                { _ => _.Type, IdentityEC },
                { _ => _.Target, LabelTargetEC },
                { _ => _.DefaultValue, SubexpressionEC },
            };
            LambdaExpressionEC = new ComponentsEqualityComparer<LambdaExpression>
            {
                { _ => _.TailCall, OperatorEC<Boolean>() },
                { _ => _.Parameters, SequenceEC(ParameterExpressionEC) },
                { _ => _.Body, SubexpressionEC },
            };
            ListInitExpressionEC = new ComponentsEqualityComparer<ListInitExpression>
            {
                { _ => _.NewExpression, NewExpressionEC },
                { _ => _.Initializers, SequenceEC(ElementInitEC) },
            };
            LoopExpressionEC = new ComponentsEqualityComparer<LoopExpression>
            {
                { _ => _.Type, IdentityEC },
                { _ => _.BreakLabel, LabelTargetEC },
                { _ => _.ContinueLabel, LabelTargetEC },
                { _ => _.Body, SubexpressionEC },
            };
            MemberAssignmentEC = new ComponentsEqualityComparer<MemberAssignment>
            {
                { _ => _.Member, IdentityEC },
                { _ => _.Expression, SubexpressionEC },
            };
            MemberBindingEC = new VariantsEqualityComparer<MemberBinding>(mb => MemberBindingVariants[(Int32)mb.BindingType])
            {
                MemberAssignmentEC,
                MemberListBindingEC,
                MemberMemberBindingEC,
            };
            MemberExpressionEC = new ComponentsEqualityComparer<MemberExpression>
            {
                { _ => _.Member, IdentityEC },
                { _ => _.Expression, SubexpressionEC },
            };
            MemberInitExpressionEC = new ComponentsEqualityComparer<MemberInitExpression>
            {
                { _ => _.NewExpression, NewExpressionEC },
                { _ => _.Bindings, SequenceEC(MemberBindingEC) },
            };
            MemberListBindingEC = new ComponentsEqualityComparer<MemberListBinding>
            {
                { _ => _.Member, IdentityEC },
                { _ => _.Initializers, SequenceEC(ElementInitEC) },
            };
            MemberMemberBindingEC = new ComponentsEqualityComparer<MemberMemberBinding>
            {
                { _ => _.Member, IdentityEC },
                { _ => _.Bindings, SequenceEC(NoInlineEC(MemberBindingEC)) },
            };
            MethodCallExpressionEC = new ComponentsEqualityComparer<MethodCallExpression>
            {
                { _ => _.Method, IdentityEC },
                { _ => _.Object, SubexpressionEC },
                { _ => _.Arguments, SequenceEC(SubexpressionEC) },
            };
            NewArrayExpressionEC = new ComponentsEqualityComparer<NewArrayExpression>
            {
                { _ => _.Type, IdentityEC },
                { _ => _.Expressions, SequenceEC(SubexpressionEC) },
            };
            NewExpressionEC = new ComponentsEqualityComparer<NewExpression>
            {
                { _ => _.Constructor, IdentityEC },
                { _ => _.Members, NullEC(SequenceEC(IdentityEC)) },
                { _ => _.Arguments, SequenceEC(SubexpressionEC) },
            };
            RuntimeVariablesExpressionEC = new ComponentsEqualityComparer<RuntimeVariablesExpression>
            {
                { _ => _.Variables, SequenceEC(ParameterExpressionEC) },
            };
            SwitchCaseEC = new ComponentsEqualityComparer<SwitchCase>
            {
                { _ => _.Body, SubexpressionEC },
                { _ => _.TestValues, MultiSetEC(SubexpressionEC) },
            };
            SwitchExpressionEC = new ComponentsEqualityComparer<SwitchExpression>
            {
                { _ => _.Comparison, IdentityEC },
                { _ => _.SwitchValue, SubexpressionEC },
                { _ => _.DefaultBody, SubexpressionEC },
                { _ => _.Cases, MultiSetEC(SwitchCaseEC) },
            };
            TryExpressionEC = new ComponentsEqualityComparer<TryExpression>
            {
                { _ => _.Type, IdentityEC },
                { _ => _.Body, SubexpressionEC },
                { _ => _.Fault, SubexpressionEC },
                { _ => _.Finally, SubexpressionEC },
                { _ => _.Handlers, SequenceEC(CatchBlockEC) },
            };
            TypeBinaryExpressionEC = new ComponentsEqualityComparer<TypeBinaryExpression>
            {
                { _ => _.TypeOperand, IdentityEC },
                { _ => _.Expression, SubexpressionEC },
            };
            UnaryExpressionEC = new ComponentsEqualityComparer<UnaryExpression>
            {
                { _ => _.NodeType, OperatorEC<ExpressionType>() },
                { _ => _.Type, IdentityEC },
                { _ => _.Method, IdentityEC },
                { _ => _.Operand, SubexpressionEC },
            };
        }
    }
}
