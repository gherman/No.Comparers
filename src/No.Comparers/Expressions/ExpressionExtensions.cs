using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers.Expressions
{
    public static class ExpressionExtensions
    {
        private sealed class SubstituteParametersVisitor : ExpressionVisitor
        {
            public IReadOnlyCollection<ParameterExpression> Parameters { get; set; }
            public IReadOnlyList<Expression> Replacements { get; set; }

            protected override Expression VisitParameter(ParameterExpression node)
            {
                int index = 0;
                foreach (var parameter in Parameters)
                {
                    if (node == parameter)
                        return Replacements[index];
                    index++;
                }
                return base.VisitParameter(node);
            }
        }

        public static Expression SubstituteParametersWith(this LambdaExpression lambda, params Expression[] replacements)
        {
            return new SubstituteParametersVisitor { Parameters = lambda.Parameters, Replacements = replacements }.Visit(lambda.Body);
        }
    }
}
