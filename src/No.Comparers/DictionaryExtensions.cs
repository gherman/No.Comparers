using System;
using System.Collections.Generic;

namespace No.Comparers
{
    public static class DictionaryExtensions
    {
        public static TValue GetOrAdd<TKey, TValue, TSubValue>(this IDictionary<TKey, TValue> dictionary, TKey key, Func<TKey, TSubValue> factory)
            where TSubValue : TValue
        {
            TValue value;
            if (!dictionary.TryGetValue(key, out value))
            {
                value = factory(key);
                dictionary.Add(key, value);
            }
            return value;
        }
    }
}
