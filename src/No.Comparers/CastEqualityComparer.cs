using System;
using System.Collections.Generic;

namespace No.Comparers
{
    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<TSource> CastEC<TSource, TTarget>(IEqualityComparer<TTarget> targetEC)
            where TTarget : TSource =>
            new ComponentsEqualityComparer<TSource>
            {
                { _ => (TTarget)_, targetEC },
            };
    }
}
