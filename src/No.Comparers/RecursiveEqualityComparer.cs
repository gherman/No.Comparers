using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers
{
    public class RecursiveEqualityComparer<T> : InliningEqualityComparer<T>
    {
        private readonly IEqualityComparer<T> Inner;

        public RecursiveEqualityComparer(Func<IEqualityComparer<T>, IEqualityComparer<T>> factory)
        {
            Inner = factory(new NoInlineEqualityComparer<T>(this));
        }

        public override Expression EqualsExpression(Expression x, Expression y) =>
            Inner.EqualsExpression(x, y);

        public override Expression GetHashCodeExpression(Expression obj) =>
            Inner.GetHashCodeExpression(obj);
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<T> RecursiveEC<T>(Func<IEqualityComparer<T>, IEqualityComparer<T>> factory) =>
            new RecursiveEqualityComparer<T>(factory);
    }
}
