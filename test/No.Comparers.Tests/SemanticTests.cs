﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NUnit.Framework;

namespace No.Comparers.Tests
{
    using static EqualityComparerFactories;

    public interface SemanticSuiteConsumer<out TResult>
    {
        TResult Consume<T>(IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses);
    }

    public abstract class SemanticSuite
    {
        public abstract String Name { get; }
        public abstract TResult Accept<TResult>(SemanticSuiteConsumer<TResult> consumer);

        public sealed override String ToString()
        {
            return Name;
        }

        private class ItemTypeQuery : SemanticSuiteConsumer<Type>
        {
            public Type Consume<T>(IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses)
            {
                return typeof(T);
            }
        }

        public Type ItemType =>
            Accept(new ItemTypeQuery());

        public static IEnumerable<SemanticSuite> CreateDeep(Int32 depth)
        {
            if (depth == 0)
            {
                yield return new BuiltinOperatorSuite();
                yield return new UserOperatorSuite();
                yield return new EquatableSuite();
                yield return new ObjectSuite();
                yield return new IdentitySuite();
            }
            else
            {
                foreach (var inner in CreateDeep(depth - 1))
                {
                    yield return new WrappingSemanticSuite<ArrayWrapping>(inner);
                    yield return new WrappingSemanticSuite<HashCodeWrapping>(inner);
                    yield return new WrappingSemanticSuite<KeyValuePairWrapping>(inner);
                    yield return new WrappingSemanticSuite<SequenceWrapping>(inner);
                    yield return new WrappingSemanticSuite<MultiSetWrapping>(inner);
                    yield return new WrappingSemanticSuite<UniqueHashCodeWrapping>(inner);
                    if (!inner.ItemType.GetTypeInfo().IsValueType)
                        yield return new WrappingSemanticSuite<MemoizingWrapping>(inner);
                }
            }
        }

        public static IEnumerable<SemanticSuite> CreateAll()
        {
            return Enumerable.Range(0, 3).SelectMany(CreateDeep);
        }
    }

    [TestFixture(typeof(Equality))]
    [TestFixture(typeof(Inequality))]
    [TestFixture(typeof(HashCodeEquality))]
    [TestFixture(typeof(HashCodeDispersion))]
    public class SemanticTest<TTest>
        where TTest : SemanticSuiteConsumer<String>, new()
    {
        [Test]
        public void Test([ValueSource(typeof(SemanticSuite), nameof(SemanticSuite.CreateAll))] SemanticSuite suite)
        {
            var result = suite.Accept(new TTest());
            //Assert.Pass(result); // TODO: show test information without Assert.Pass (confuses some test result readers)
        }
    }

    public class Equality : SemanticSuiteConsumer<String>
    {
        public Int32 MaxCount { get; } = 1000;

        public String Consume<T>(IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses)
        {
            Int32 count = 0;
            foreach (var @class in equivalenceClasses)
            {
                var values = new List<T>();
                foreach (var value in @class)
                {
                    foreach (var other in values)
                    {
                        Assert.IsTrue(comparer.Equals(value, other));
                        if (++count == MaxCount)
                            return String.Empty;
                    }
                    values.Add(value);
                }
                if (++count == MaxCount)
                    return String.Empty;
            }
            return String.Format("value comparisons: {0}", count);
        }
    }

    public class Inequality : SemanticSuiteConsumer<String>
    {
        public Int32 MaxCount { get; } = 1000;

        public String Consume<T>(IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses)
        {
            Int32 count = 0;
            var classes = new List<IEnumerable<T>>();
            foreach (var @class in equivalenceClasses)
            {
                foreach (var x in @class)
                    foreach (var other in classes)
                        foreach (var y in other)
                        {
                            Assert.IsFalse(comparer.Equals(x, y));
                            if (++count == MaxCount)
                                return String.Empty;
                        }
                classes.Add(@class);
                if (++count == MaxCount)
                    return String.Empty;
            }
            return String.Format("value comparisons: {0}", count);
        }
    }

    public class HashCodeEquality : SemanticSuiteConsumer<String>
    {
        public Int32 MaxCount { get; } = 1000;

        public String Consume<T>(IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses)
        {
            Int32 count = 0;
            foreach (var @class in equivalenceClasses)
            {
                var expected = comparer.GetHashCode(@class.First());
                foreach (var value in @class.Skip(1))
                {
                    Assert.AreEqual(expected, comparer.GetHashCode(value));
                    if (++count == MaxCount)
                        return String.Empty;
                }
                if (++count == MaxCount)
                    return String.Empty;
            }
            return String.Format("hash code comparisons: {0}", count);
        }
    }

    public class HashCodeDispersion : SemanticSuiteConsumer<String>
    {
        public Int32 MaxCount { get; } = 1000;

        public String Consume<T>(IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses)
        {
            Int32 count = 0;
            Int32 maxValue = 0;
            var buckets = new Dictionary<Int32, List<T>>();
            foreach (var @class in equivalenceClasses)
            {
                var item = @class.First();
                var hashCode = comparer.GetHashCode(item);
                List<T> bucket;
                if (!buckets.TryGetValue(hashCode, out bucket))
                {
                    bucket = new List<T>();
                    buckets.Add(hashCode, bucket);
                }
                bucket.Add(item);
                maxValue = Math.Max(maxValue, bucket.Count);
                if (++count == MaxCount)
                    break;
            }
            Assert.LessOrEqual(maxValue, 3);
            return String.Format("maximum hash code density: {0} (in {1} elements)", maxValue, count);
        }
    }

    public interface WrappingSemanticSuiteConsumer
    {
        String NameSuffix { get; }
        TResult Consume<TResult, T>(SemanticSuiteConsumer<TResult> inner, IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses);
    }

    public class WrappingSemanticSuite<TWrapping> : SemanticSuite
        where TWrapping : WrappingSemanticSuiteConsumer, new()
    {
        public SemanticSuite Inner { get; }
        public TWrapping Wrapping { get; }

        public WrappingSemanticSuite(SemanticSuite inner)
        {
            Inner = inner;
            Wrapping = new TWrapping();
        }

        public override String Name =>
            Inner.Name + "+" + Wrapping.NameSuffix;

        private class WrappedConsumer<TResult> : SemanticSuiteConsumer<TResult>
        {
            public SemanticSuiteConsumer<TResult> Inner { get; set; }
            public TWrapping Wrapping { get; set; }

            public TResult Consume<T>(IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses)
            {
                return Wrapping.Consume(Inner, comparer, equivalenceClasses);
            }
        }

        public override TResult Accept<TResult>(SemanticSuiteConsumer<TResult> consumer)
        {
            return Inner.Accept(new WrappedConsumer<TResult> { Inner = consumer, Wrapping = Wrapping });
        }
    }

    public class HashCodeWrapping : WrappingSemanticSuiteConsumer
    {
        public String NameSuffix => "HashCode";

        public TResult Consume<TResult, T>(SemanticSuiteConsumer<TResult> inner, IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses)
        {
            return inner.Consume(HashCodeEC(comparer), equivalenceClasses);
        }
    }

    public class MemoizingWrapping : WrappingSemanticSuiteConsumer
    {
        public String NameSuffix => "Memoizing";

        public TResult Consume<TResult, T>(SemanticSuiteConsumer<TResult> inner, IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses)
        {
            return inner.Consume(MemoizingECChecked(comparer), equivalenceClasses);
        }
    }

    public class KeyValuePairWrapping : WrappingSemanticSuiteConsumer
    {
        public String NameSuffix => "KeyValuePair";

        public TResult Consume<TResult, T>(SemanticSuiteConsumer<TResult> inner, IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses)
        {
            return inner.Consume(KeyValuePairEC(comparer, comparer), Combine(equivalenceClasses));
        }

        private static IEnumerable<IEnumerable<KeyValuePair<T, T>>> Combine<T>(IEnumerable<IEnumerable<T>> equivalenceClasses)
        {
            var classes = new List<IEnumerable<T>>();
            foreach (var @class in equivalenceClasses)
            {
                // pairs with equal components
                var equalPairs = new List<KeyValuePair<T, T>>();
                foreach (var x in @class)
                    foreach (var y in @class)
                        equalPairs.Add(new KeyValuePair<T, T>(x, y));
                yield return equalPairs;
                // pairs with unequal components
                foreach (var otherClass in classes)
                {
                    var unequalPairs = new List<KeyValuePair<T, T>>();
                    foreach (var x in @class)
                        foreach (var y in @otherClass)
                            unequalPairs.Add(new KeyValuePair<T, T>(x, y));
                    yield return unequalPairs;
                }
                classes.Add(@class);
            }
        }
    }

    public class SequenceWrapping : WrappingSemanticSuiteConsumer
    {
        public virtual String NameSuffix => "Sequence";

        public virtual TResult Consume<TResult, T>(SemanticSuiteConsumer<TResult> inner, IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses)
        {
            return inner.Consume(SequenceEC(comparer), Combine(equivalenceClasses));
        }

        protected static IEnumerable<IEnumerable<T[]>> Combine<T>(IEnumerable<IEnumerable<T>> equivalenceClasses)
        {
            const Int32 MaxLength = 3;
            yield return new[] { new T[0] };
            var classes = new List<IEnumerable<T>>();
            foreach (var @class in equivalenceClasses)
            {
                // sequences with one element, possibly repeated
                for (var repeats = 1; repeats <= MaxLength; repeats++)
                {
                    var sequences = new List<T[]>();
                    foreach (var x in @class)
                        sequences.Add(Enumerable.Repeat(x, repeats).ToArray());
                    yield return sequences;
                }
                // pairs with two different elements
                foreach (var otherClass in classes)
                    for (var xrepeats = 1; xrepeats < MaxLength; xrepeats++)
                        for (var yrepeats = 1; xrepeats + yrepeats <= MaxLength; yrepeats++)
                        {
                            var sequences = new List<T[]>();
                            foreach (var x in @class)
                                foreach (var y in @otherClass)
                                    sequences.Add(Enumerable.Repeat(x, xrepeats).Concat(Enumerable.Repeat(y, yrepeats)).ToArray());
                            yield return sequences;
                            sequences = new List<T[]>();
                            foreach (var x in @class)
                                foreach (var y in @otherClass)
                                    sequences.Add(Enumerable.Repeat(y, yrepeats).Concat(Enumerable.Repeat(x, xrepeats)).ToArray());
                            yield return sequences;
                        }
                classes.Add(@class);
            }
        }
    }

    public class ArrayWrapping : SequenceWrapping
    {
        public override String NameSuffix => "Array";

        public override TResult Consume<TResult, T>(SemanticSuiteConsumer<TResult> inner, IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses)
        {
            return inner.Consume(ArrayEC(comparer), Combine(equivalenceClasses));
        }
    }

    public class MultiSetWrapping : WrappingSemanticSuiteConsumer
    {
        public String NameSuffix => "MultiSet";

        public TResult Consume<TResult, T>(SemanticSuiteConsumer<TResult> inner, IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses)
        {
            return inner.Consume(MultiSetEC(comparer), Combine(equivalenceClasses));
        }

        private static IEnumerable<IEnumerable<IReadOnlyCollection<T>>> Combine<T>(IEnumerable<IEnumerable<T>> equivalenceClasses)
        {
            const Int32 MaxLength = 3;
            yield return new[] { new T[0] as IReadOnlyCollection<T> };
            var classes = new List<IEnumerable<T>>();
            foreach (var @class in equivalenceClasses)
            {
                // sequences with one element, possibly repeated
                for (var repeats = 1; repeats <= MaxLength; repeats++)
                {
                    var sequences = new List<List<T>>();
                    foreach (var x in @class)
                        sequences.Add(Enumerable.Repeat(x, repeats).ToList());
                    yield return sequences;
                }
                // pairs with two different elements
                foreach (var otherClass in classes)
                    for (var xrepeats = 1; xrepeats < MaxLength; xrepeats++)
                        for (var yrepeats = 1; xrepeats + yrepeats <= MaxLength; yrepeats++)
                        {
                            var sequences = new List<List<T>>();
                            foreach (var x in @class)
                                foreach (var y in @otherClass)
                                    sequences.Add(Enumerable.Repeat(x, xrepeats).Concat(Enumerable.Repeat(y, yrepeats)).ToList());
                            foreach (var x in @class)
                                foreach (var y in @otherClass)
                                    sequences.Add(Enumerable.Repeat(y, yrepeats).Concat(Enumerable.Repeat(x, xrepeats)).ToList());
                            yield return sequences;
                        }
                classes.Add(@class);
            }
        }
    }

    public class UniqueHashCodeWrapping : WrappingSemanticSuiteConsumer
    {
        public String NameSuffix => "UniqueHashCode";

        public TResult Consume<TResult, T>(SemanticSuiteConsumer<TResult> inner, IEqualityComparer<T> comparer, IEnumerable<IEnumerable<T>> equivalenceClasses)
        {
            return inner.Consume(UniqueHashCodeEC(comparer), equivalenceClasses);
        }
    }

    public abstract class SemanticSuite<T> : SemanticSuite
    {
        public abstract IEnumerable<IEnumerable<T>> EquivalenceClasses { get; }
        public abstract IEqualityComparer<T> Comparer { get; }

        public sealed override TResult Accept<TResult>(SemanticSuiteConsumer<TResult> consumer)
        {
            return consumer.Consume(Comparer, EquivalenceClasses);
        }
    }

    public class BuiltinOperatorSuite : SemanticSuite<Int32>
    {
        public override String Name =>
            "BuiltinOperator";

        public override IEqualityComparer<Int32> Comparer =>
            OperatorEC<Int32>();

        public override IEnumerable<IEnumerable<Int32>> EquivalenceClasses =>
            from value in Enumerable.Range(0, Int32.MaxValue) select Enumerable.Repeat(value, 1);
    }

    public class UserOperatorSuite : SemanticSuite<UserOperatorSuite.Item>
    {
        public class Item
        {
            public Int32 Value { get; set; }

            public override Boolean Equals(Object other)
            {
                return base.Equals(other);
            }

            public override Int32 GetHashCode()
            {
                return Value.GetHashCode();
            }

            public static Boolean operator ==(Item x, Item y)
            {
                return x.Value == y.Value;
            }

            public static Boolean operator !=(Item x, Item y)
            {
                return x.Value != y.Value;
            }
        }

        public override String Name =>
            "UserOperator";

        public override IEqualityComparer<Item> Comparer =>
            OperatorEC<Item>();

        public override IEnumerable<IEnumerable<Item>> EquivalenceClasses =>
            from value in Enumerable.Range(0, Int32.MaxValue) select new[] { new Item { Value = value }, new Item { Value = value } };
    }

    public class EquatableSuite : SemanticSuite<EquatableSuite.Item>
    {
        public class Item : IEquatable<Item>
        {
            public Int32 X { get; set; }
            public Int32 Y { get; set; }

            public Boolean Equals(Item other)
            {
                return X == other.X;
            }

            public override Boolean Equals(Object obj)
            {
                return true;
            }

            public override Int32 GetHashCode()
            {
                return X.GetHashCode();
            }
        }

        public override String Name =>
            "Equatable";

        public override IEqualityComparer<Item> Comparer =>
            EquatableEC<Item>();

        public override IEnumerable<IEnumerable<Item>> EquivalenceClasses =>
            from value in Enumerable.Range(0, Int32.MaxValue) select new[] { new Item { X = value, Y = 0 }, new Item { X = value, Y = 1 } };
    }

    public class ObjectSuite : SemanticSuite<ObjectSuite.Item>
    {
        public class Item
        {
            public Int32 X { get; set; }
            public Int32 Y { get; set; }

            public override Boolean Equals(Object obj)
            {
                if (obj is Item)
                    return X == ((Item)obj).X;
                return false;
            }

            public override Int32 GetHashCode()
            {
                return X.GetHashCode();
            }
        }

        public override String Name =>
            "Object";

        public override IEqualityComparer<Item> Comparer =>
            ObjectEC<Item>();

        public override IEnumerable<IEnumerable<Item>> EquivalenceClasses =>
            from value in Enumerable.Range(0, Int32.MaxValue) select new[] { new Item { X = value, Y = 0 }, new Item { X = value, Y = 1 } };
    }

    public class IdentitySuite : SemanticSuite<IdentitySuite.Item>
    {
        public class Item
        {
            public override Boolean Equals(Object obj)
            {
                return true;
            }

            public override Int32 GetHashCode()
            {
                return 0;
            }
        }

        public override String Name =>
            "Identity";

        public override IEqualityComparer<Item> Comparer =>
            IdentityEC<Item>();

        public override IEnumerable<IEnumerable<Item>> EquivalenceClasses =>
            from value in Enumerable.Range(0, Int32.MaxValue) select new[] { new Item() };
    }
}
