using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers
{
    using static EqualityComparerFactories;
    using static Expression;
    using static Expressions.ComplexExpression;

    public class DictionaryEqualityComparer<TKey, TValue> : InliningEqualityComparer<IReadOnlyDictionary<TKey, TValue>>
    {
        private readonly IEqualityComparer<TValue> ValueEC;
        private readonly IEqualityComparer<IEnumerable<KeyValuePair<TKey, TValue>>> KvpSetEC;

        public DictionaryEqualityComparer(IEqualityComparer<TKey> keyEC, IEqualityComparer<TValue> valueEC)
        {
            if (keyEC == null)
                throw new ArgumentNullException(nameof(keyEC));
            if (valueEC == null)
                throw new ArgumentNullException(nameof(valueEC));
            ValueEC = valueEC;
            KvpSetEC = MultiSetEC(KeyValuePairEC(keyEC, ValueEC));
        }

        public override Expression EqualsExpression(Expression x, Expression y)
        {
            var tempx = Variable(typeof(IReadOnlyDictionary<TKey, TValue>));
            var tempy = Variable(typeof(IReadOnlyDictionary<TKey, TValue>));
            var value = Variable(typeof(TValue));
            var @return = Label(typeof(Boolean));
            return Block(
                typeof(Boolean),
                new[] { tempx, tempy },
                Assign(
                    tempx,
                    x),
                Assign(
                    tempy,
                    y),
                IfThen(
                    NotEqual(
                        Property(
                            tempx,
                            Reflections.IReadOnlyCollection<KeyValuePair<TKey, TValue>>.Count),
                        Property(
                            tempy,
                            Reflections.IReadOnlyCollection<KeyValuePair<TKey, TValue>>.Count)),
                    Break(
                        @return,
                        Constant(false))),
                ForEach<KeyValuePair<TKey, TValue>>(tempx, item =>
                    Block(
                        new[] { value },
                        IfThen(
                            OrElse(
                                Not(
                                    Call(
                                        tempy,
                                        Reflections.IReadOnlyDictionary<TKey, TValue>.TryGetValue,
                                        Property(
                                            item,
                                            Reflections.KeyValuePair<TKey, TValue>.Key),
                                        value)),
                                Not(
                                    ValueEC.EqualsExpression(
                                        Property(
                                            item,
                                            Reflections.KeyValuePair<TKey, TValue>.Value),
                                        value))),
                            Break(
                                @return,
                                Constant(false))))),
                Label(
                    @return,
                    Constant(true)));
        }

        public override Expression GetHashCodeExpression(Expression obj) =>
            KvpSetEC.GetHashCodeExpression(obj);
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<IReadOnlyDictionary<TKey, TValue>> DictionaryEC<TKey, TValue>(IEqualityComparer<TKey> keyEC, IEqualityComparer<TValue> valueEC) =>
            new DictionaryEqualityComparer<TKey, TValue>(keyEC, valueEC);
    }
}
