using System;
using System.Collections.Generic;

namespace No.Comparers
{
    public class MagicNumbers
    {
        protected static Int32 StringHash(String s)
        {
            var hash = 1;
            foreach (var c in s)
                hash = 17 * hash + c;
            return hash;
        }
    }

    public class MagicNumbers<T> : MagicNumbers
    {
        private static readonly Random Rng;
        private static readonly List<Int32> Values;

        static MagicNumbers()
        {
            Rng = new Random(StringHash(typeof(T).AssemblyQualifiedName));
            Values = new List<Int32>();
        }

        public static Int32 Get(Int32 index)
        {
            while (Values.Count <= index)
                Values.Add(Rng.Next() | 1);
            return Values[index];
        }
    }
}
