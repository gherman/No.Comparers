using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers.Expressions
{
    using static Expression;

    /// <summary>
    /// An ExpressionVisitor replacing constants with references to a prescribed array.
    /// </summary>
    public sealed class ConstantExtractingVisitor : ExpressionVisitor
    {
        private readonly Expression Root;
        private readonly IList<Object> Values;

        public ConstantExtractingVisitor(Expression root, IList<Object> values)
        {
            Root = root;
            Values = values;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            var index = Values.Count;
            Values.Add(node.Value);
            return Convert(ArrayIndex(Root, Constant(index)), node.Type);
        }
    }
}
