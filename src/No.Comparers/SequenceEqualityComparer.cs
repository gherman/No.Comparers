using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers
{
    using static Expression;
    using static Expressions.ComplexExpression;

    public class SequenceEqualityComparer<T> : InliningEqualityComparer<IEnumerable<T>>
    {
        private readonly IEqualityComparer<T> ItemEC;

        public SequenceEqualityComparer(IEqualityComparer<T> itemEC)
        {
            if (itemEC == null)
                throw new ArgumentNullException(nameof(itemEC));
            ItemEC = itemEC;
        }

        public override Expression EqualsExpression(Expression x, Expression y)
        {
            var hasNextX = Variable(typeof(Boolean));
            var hasNextY = Variable(typeof(Boolean));
            var @break = Label();
            var @return = Label(typeof(Boolean));
            return Using(
                Call(
                    x,
                    Reflections.IEnumerable<T>.GetEnumerator),
                Call(
                    y,
                    Reflections.IEnumerable<T>.GetEnumerator),
                (enumeratorX, enumeratorY) =>
                    Block(
                        typeof(Boolean),
                        Loop(
                            Block(
                                new[] { hasNextX, hasNextY },
                                Assign(
                                    hasNextX,
                                    Call(
                                        enumeratorX,
                                        Reflections.IEnumerator.MoveNext)),
                                Assign(
                                    hasNextY,
                                    Call(
                                        enumeratorY,
                                        Reflections.IEnumerator.MoveNext)),
                                IfThen(
                                    NotEqual(
                                        hasNextX,
                                        hasNextY),
                                    Break(
                                        @return,
                                        Constant(false))),
                                IfThen(
                                    Not(
                                        hasNextX),
                                    Break(@break)),
                                IfThen(
                                    Not(
                                        ItemEC.EqualsExpression(
                                            Property(
                                                enumeratorX,
                                                Reflections.IEnumerator<T>.Current),
                                            Property(
                                                enumeratorY,
                                                Reflections.IEnumerator<T>.Current))),
                                    Break(
                                        @return,
                                        Constant(false)))),
                            @break),
                        Label(
                            @return,
                            Constant(true))));
        }

        public override Expression GetHashCodeExpression(Expression obj)
        {
            var result = Variable(typeof(Int32));
            return Block(
                typeof(Int32),
                new[] { result },
                Assign(
                    result,
                    Constant(MagicNumbers<IEnumerable<T>>.Get(0))),
                ForEach<T>(obj, item =>
                    Assign(
                        result,
                        Add(
                            Multiply(
                                Constant(MagicNumbers<IEnumerable<T>>.Get(1)),
                                result),
                            ExclusiveOr(
                                Constant(MagicNumbers<IEnumerable<T>>.Get(2)),
                                ItemEC.GetHashCodeExpression(
                                    item))))),
                result);
        }
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<IEnumerable<T>> SequenceEC<T>(IEqualityComparer<T> itemEC) =>
            new SequenceEqualityComparer<T>(itemEC);
    }
}
