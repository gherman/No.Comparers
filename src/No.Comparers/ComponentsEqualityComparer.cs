using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace No.Comparers
{
    using Expressions;
    using static EqualityComparerFactories;
    using static Expression;

    public class ComponentsEqualityComparer<T> : InliningEqualityComparer<T>, IEnumerable
    {
        public void Add<TComponent>(Expression<Func<T, TComponent>> selector, IEqualityComparer<TComponent> comparer, Boolean useInHashCode = true)
        {
            if (Frozen)
                throw new InvalidOperationException($"Cannot add components to a {nameof(ComponentsEqualityComparer<T>)} after it has been used.");
            if (selector == null)
                throw new ArgumentNullException(nameof(selector));
            if (comparer == null)
                throw new ArgumentNullException(nameof(comparer));
            ProcessComponents += _ => _.Process(selector, comparer, useInHashCode);
        }

        private interface IBuilder
        {
            void Process<TComponent>(Expression<Func<T, TComponent>> selector, IEqualityComparer<TComponent> component, Boolean useInHashCode);
        }

        private Boolean Frozen;
        private Action<IBuilder> ProcessComponents =
            _ => { };

        private sealed class EqualsBuilder : IBuilder
        {
            private readonly Expression X;
            private readonly Expression Y;
            private readonly List<Expression> Conditions;

            public EqualsBuilder(Expression x, Expression y)
            {
                X = x;
                Y = y;
                Conditions = new List<Expression>();
            }

            public void Process<TComponent>(Expression<Func<T, TComponent>> selector, IEqualityComparer<TComponent> comparer, Boolean useInHashCode) =>
                Conditions.Add(
                    comparer.EqualsExpression(
                        selector.SubstituteParametersWith(X),
                        selector.SubstituteParametersWith(Y)));

            public Expression Result =>
                Conditions.Aggregate(Constant(true) as Expression, AndAlso);
        }

        public override Expression EqualsExpression(Expression x, Expression y)
        {
            Frozen = true;
            var builder = new EqualsBuilder(x, y);
            ProcessComponents(builder);
            return builder.Result;
        }

        private sealed class GetHashCodeBuilder : IBuilder
        {
            private readonly Expression Obj;
            private Int32 Index;
            private List<Expression> Summands;

            public GetHashCodeBuilder(Expression obj)
            {
                Obj = obj;
                Index = 1;
                Summands = new List<Expression>();
            }

            public void Process<TComponent>(Expression<Func<T, TComponent>> selector, IEqualityComparer<TComponent> comparer, Boolean useInHashCode)
            {
                if (useInHashCode)
                    Summands.Add(
                        Multiply(
                            Constant(MagicNumbers<T>.Get(Index++)),
                            comparer.GetHashCodeExpression(
                                selector.SubstituteParametersWith(Obj))));
            }

            public Expression Result =>
                Summands.Aggregate(Constant(MagicNumbers<T>.Get(0)) as Expression, Expression.Add);
        }

        public override Expression GetHashCodeExpression(Expression obj)
        {
            Frozen = true;
            var builder = new GetHashCodeBuilder(obj);
            ProcessComponents(builder);
            return builder.Result;
        }

        IEnumerator IEnumerable.GetEnumerator() =>
            throw new InvalidOperationException($"{nameof(ComponentsEqualityComparer<T>)} is not enumerable. It implements IEnumerable only to support collection initialization syntax.");
    }
}
