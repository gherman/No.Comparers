using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace No.Comparers.Expressions
{
    using static EqualityComparerFactories;
    using static Expression;

    public class NormalizingVisitor : ExpressionVisitor
    {
        private class Scope
        {
            public Scope Parent { get; }
            public IReadOnlyCollection<ParameterExpression> Variables { get; }
            public Int32 Height { get; set; }
            public HashSet<ParameterExpression> Unused { get; set; }
            public Dictionary<ParameterExpression, ParameterExpression> Used { get; set; }

            public Scope(Scope parent, IReadOnlyCollection<ParameterExpression> variables)
            {
                Parent = parent;
                Variables = variables;
            }
        }

        private Int32 NodeDepth;
        private Scope CurrentScope;
        private readonly Dictionary<ParameterExpression, Stack<Scope>> Declarations;
        private Int32 AnalysisDepth;
        private readonly Dictionary<Object, Scope> AnalyzedScopes;
        private readonly Dictionary<Tuple<Int32, Int32, Type>, ParameterExpression> NormalizedParameters;
        private readonly Dictionary<LabelTarget, Tuple<Int32, Int32, Type>> AnalyzedTargets;
        private readonly Dictionary<Tuple<Int32, Int32, Type>, LabelTarget> NormalizedLabelTargets;

        public NormalizingVisitor()
        {
            NodeDepth = 0;
            CurrentScope = null;
            Declarations = new Dictionary<ParameterExpression, Stack<Scope>>();
            AnalysisDepth = 0;
            AnalyzedScopes = new Dictionary<Object, Scope>();
            NormalizedParameters = new Dictionary<Tuple<Int32, Int32, Type>, ParameterExpression>(TupleEC(OperatorEC<Int32>(), OperatorEC<Int32>(), IdentityEC<Type>()));
            AnalyzedTargets = new Dictionary<LabelTarget, Tuple<Int32, Int32, Type>>();
            NormalizedLabelTargets = new Dictionary<Tuple<Int32, Int32, Type>, LabelTarget>(TupleEC(OperatorEC<Int32>(), OperatorEC<Int32>(), IdentityEC<Type>()));
        }

        private sealed class NodeVisitor : IDisposable
        {
            private readonly NormalizingVisitor Owner;

            public NodeVisitor(NormalizingVisitor owner)
            {
                Owner = owner;
                if (Owner.NodeDepth == 0)
                    Owner.AnalyzedTargets.Clear();
                Owner.NodeDepth++;
            }

            public void Dispose()
            {
                Owner.NodeDepth--;
            }
        }

        public override Expression Visit(Expression node)
        {
            using (new NodeVisitor(this))
                return base.Visit(node);
        }

        private abstract class ScopeVisitor : IDisposable
        {
            protected NormalizingVisitor Owner { get; }
            protected Scope Scope { get; }

            protected ScopeVisitor(NormalizingVisitor owner, Scope scope)
            {
                Owner = owner;
                Scope = scope;
                Owner.CurrentScope = Scope;
                foreach (var variable in Scope.Variables)
                    Owner.Declarations.GetOrAdd(variable, _ => new Stack<Scope>()).Push(Scope);
            }

            public virtual void Dispose()
            {
                foreach (var variable in Scope.Variables)
                    Owner.Declarations[variable].Pop();
                Owner.CurrentScope = Scope.Parent;
            }
        }

        private sealed class AnalyzingScopeVisitor : ScopeVisitor
        {
            public AnalyzingScopeVisitor(NormalizingVisitor owner, Scope scope)
                : base(owner, scope)
            {
                Scope.Height = 0;
                Owner.AnalysisDepth++;
            }

            public override void Dispose()
            {
                Owner.AnalysisDepth--;
                if (Scope.Parent != null)
                    Scope.Parent.Height = Math.Max(Scope.Parent.Height, Scope.Height + 1);
                base.Dispose();
            }
        }

        private sealed class RewritingScopeVisitor : ScopeVisitor
        {
            public RewritingScopeVisitor(NormalizingVisitor owner, Scope scope)
                : base(owner, scope)
            {
                Scope.Unused = new HashSet<ParameterExpression>(Scope.Variables);
                Scope.Used = new Dictionary<ParameterExpression, ParameterExpression>();
            }
        }

        private Scope AnalyzeScope<TNode, TIgnored>(TNode node, Func<TNode, IReadOnlyCollection<ParameterExpression>> variablesSelector, Func<TNode, TIgnored> action)
        {
            Scope scope;
            if (!AnalyzedScopes.TryGetValue(node, out scope))
            {
                scope = new Scope(CurrentScope, variablesSelector(node));
                using (new AnalyzingScopeVisitor(this, scope))
                    action(node);
                AnalyzedScopes.Add(node, scope);
            }
            return scope;
        }

        protected override Expression VisitBlock(BlockExpression node)
        {
            var scope = AnalyzeScope(node, _ => _.Variables, base.VisitBlock);
            if (AnalysisDepth > 0)
                return node;
            using (new RewritingScopeVisitor(this, scope))
            {
                var expressions = Visit(node.Expressions);
                var variables = VisitAndConvert(node.Variables, nameof(VisitBlock)).OrderBy(RuntimeHelpers.GetHashCode);
                return Block(node.Type, variables, expressions);
            }
        }

        protected override CatchBlock VisitCatchBlock(CatchBlock node)
        {
            var scope = AnalyzeScope(node, _ => new[] { node.Variable }, base.VisitCatchBlock);
            if (AnalysisDepth > 0)
                return node;
            using (new RewritingScopeVisitor(this, scope))
                return Catch(VisitAndConvert(node.Variable, nameof(VisitCatchBlock)), Visit(node.Body), Visit(node.Filter));
        }

        protected override LabelTarget VisitLabelTarget(LabelTarget node)
        {
            if (AnalysisDepth > 0)
                return node;
            if (node == null)
                return node;
            var key = AnalyzedTargets.GetOrAdd(node, t => Tuple.Create(NodeDepth, AnalyzedTargets.Count, t.Type));
            return NormalizedLabelTargets.GetOrAdd(key, k => Label(k.Item3, $"D{k.Item1}I{k.Item2}"));
        }

        protected override Expression VisitLambda<TDelegate>(Expression<TDelegate> node)
        {
            var scope = AnalyzeScope(node, _ => _.Parameters, base.VisitLambda<TDelegate>);
            if (AnalysisDepth > 0)
                return node;
            using (new RewritingScopeVisitor(this, scope))
                return Lambda<TDelegate>(Visit(node.Body), node.TailCall, VisitAndConvert(node.Parameters, nameof(VisitLambda)));
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            if (AnalysisDepth > 0)
                return node;
            Stack<Scope> stack;
            if (!Declarations.TryGetValue(node, out stack))
                return node;
            var scope = stack.Peek();
            if (scope.Unused.Remove(node))
            {
                var key = Tuple.Create(scope.Height, scope.Used.Count, node.Type);
                scope.Used.Add(node, NormalizedParameters.GetOrAdd(key, k => Parameter(k.Item3, $"H{k.Item1}I{k.Item2}")));
            }
            return scope.Used[node];
        }
    }
}
