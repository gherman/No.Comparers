using System;
using System.Collections.Generic;

namespace No.Comparers
{
    public class EqualityComparerFromComparer<T> : IEqualityComparer<T>
    {
        private readonly IComparer<T> Comparer;
        private readonly Func<T, Int32> GetHashCodeFunc;

        public EqualityComparerFromComparer(IComparer<T> comparer, Func<T, Int32> getHashCode)
        {
            if (comparer == null)
                throw new ArgumentNullException(nameof(comparer));
            if (getHashCode == null)
                throw new ArgumentNullException(nameof(getHashCode));
            Comparer = comparer;
            GetHashCodeFunc = getHashCode;
        }

        public Boolean Equals(T x, T y)
        {
            return Comparer.Compare(x, y) == 0;
        }

        public Int32 GetHashCode(T x)
        {
            return GetHashCodeFunc(x);
        }
    }
}
