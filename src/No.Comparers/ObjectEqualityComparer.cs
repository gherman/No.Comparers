using System;
using System.Linq.Expressions;

namespace No.Comparers
{
    using System.Collections.Generic;
    using static Expression;

    public sealed class ObjectEqualityComparer<T> : IEqualityComparerEx<T>
    {
        public Boolean Equals(T x, T y) =>
            x.Equals(y);

        public Int32 GetHashCode(T obj) =>
            obj.GetHashCode();

        public Expression EqualsExpression(Expression x, Expression y) =>
            Call(x, Reflections.Object.Equals, y);

        public Expression GetHashCodeExpression(Expression obj) =>
            Call(obj, Reflections.Object.GetHashCode);
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<T> ObjectEC<T>() =>
            new ObjectEqualityComparer<T>();
    }
}
