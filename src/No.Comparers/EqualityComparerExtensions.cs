using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers
{
    using static Expression;

    internal interface IEqualityComparerEx<T> : IEqualityComparer<T>
    {
        Expression EqualsExpression(Expression x, Expression y);
        Expression GetHashCodeExpression(Expression obj);
    }

    public static partial class EqualityComparerExtensions
    {
        public static Expression EqualsExpression<T>(this IEqualityComparer<T> comparer, Expression x, Expression y)
        {
            if (comparer is IEqualityComparerEx<T> ex)
                return ex.EqualsExpression(x, y);
            if (ReferenceEquals(comparer, EqualityComparer<T>.Default))
                return DefaultEqualityComparer<T>.Instance.EqualsExpression(x, y);
            return Call(Constant(comparer), Reflections.IEqualityComparer<T>.Equals, x, y);
        }

        public static Expression EqualsExpression<T>(this IEqualityComparer<T> comparer, Expression x, Expression y, Boolean inline) =>
            inline ? comparer.EqualsExpression(x, y) : Call(Constant(comparer), Reflections.IEqualityComparer<T>.Equals, x, y);

        public static Expression GetHashCodeExpression<T>(this IEqualityComparer<T> comparer, Expression obj)
        {
            if (comparer is IEqualityComparerEx<T> ex)
                return ex.GetHashCodeExpression(obj);
            if (ReferenceEquals(comparer, EqualityComparer<T>.Default))
                return DefaultEqualityComparer<T>.Instance.GetHashCodeExpression(obj);
            return Call(Constant(comparer), Reflections.IEqualityComparer<T>.GetHashCode, obj);
        }

        public static Expression GetHashCodeExpression<T>(this IEqualityComparer<T> comparer, Expression obj, Boolean inline) =>
            inline ? comparer.GetHashCodeExpression(obj) : Call(Constant(comparer), Reflections.IEqualityComparer<T>.GetHashCode, obj);
    }
}
