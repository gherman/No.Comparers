using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers
{
    public sealed class IdentityFirstEqualityComparer<T> : InliningEqualityComparer<T>
        where T : class
    {
        private readonly IEqualityComparer<T> Inner;

        public IdentityFirstEqualityComparer(IEqualityComparer<T> inner)
        {
            Inner = inner;
        }

        public override Expression EqualsExpression(Expression x, Expression y)
        {
            var copyX = Expression.Parameter(x.Type);
            var copyY = Expression.Parameter(y.Type);
            return Expression.Block(
                new[] { copyX, copyY },
                Expression.Assign(copyX, x),
                Expression.Assign(copyY, y),
                Expression.OrElse(
                    Expression.ReferenceEqual(copyX, copyY),
                    Inner.EqualsExpression(copyX, copyY)));
        }

        public override Expression GetHashCodeExpression(Expression obj) =>
            Inner.GetHashCodeExpression(obj);
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<T> IdentityFirstEC<T>(IEqualityComparer<T> inner)
            where T : class =>
            new IdentityFirstEqualityComparer<T>(inner);
    }
}
