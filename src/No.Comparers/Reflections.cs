using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace No.Comparers
{
    #region helper methods
    public partial class Reflections
    {
        protected static ConstructorInfo Constructor(LambdaExpression expression)
        {
            return (expression.Body as NewExpression).Constructor;
        }

        protected static PropertyInfo Indexer(LambdaExpression expression)
        {
            var getter = Method(expression);
            return getter.DeclaringType.GetTypeInfo().DeclaredProperties.Single(p => p.GetMethod == getter);
        }

        protected static MethodInfo Method(LambdaExpression expression)
        {
            return (expression.Body as MethodCallExpression).Method;
        }

        protected static MethodInfo Method<TResult>(Expression<Func<TResult>> expression)
        {
            return Method(expression as LambdaExpression);
        }

        protected static MethodInfo Method(Expression<Action> expression)
        {
            return Method(expression as LambdaExpression);
        }

        protected static PropertyInfo Property(LambdaExpression expression)
        {
            return (expression.Body as MemberExpression).Member as PropertyInfo;
        }

        protected static PropertyInfo Property<TResult>(Expression<Func<TResult>> expression)
        {
            return Property(expression as LambdaExpression);
        }
    }

    public class Reflections<TInstance> : Reflections
    {
        protected static ConstructorInfo Constructor(Expression<Func<TInstance>> expression)
        {
            return Constructor(expression as LambdaExpression);
        }

        protected static PropertyInfo Indexer<TResult>(Expression<Func<TInstance, TResult>> expression)
        {
            return Indexer(expression as LambdaExpression);
        }

        protected static MethodInfo Method<TResult>(Expression<Func<TInstance, TResult>> expression)
        {
            return Method(expression as LambdaExpression);
        }

        protected static MethodInfo Method(Expression<Action<TInstance>> expression)
        {
            return Method(expression as LambdaExpression);
        }

        protected static PropertyInfo Property<TResult>(Expression<Func<TInstance, TResult>> expression)
        {
            return Property(expression as LambdaExpression);
        }
    }
    #endregion

    public partial class Reflections
    {
        public class ConditionalWeakTable<TKey, TValue> : Reflections<System.Runtime.CompilerServices.ConditionalWeakTable<TKey, TValue>>
            where TKey : class
            where TValue : class
        {
            private static TValue value;
            public static MethodInfo Add { get; } = Method(_ => _.Add(default(TKey), default(TValue)));
            public static MethodInfo TryGetValue { get; } = Method(_ => _.TryGetValue(default(TKey), out value));
        }

        public class Dictionary<TKey, TValue> : Reflections<System.Collections.Generic.Dictionary<TKey, TValue>>
        {
            private static TValue value;
            public static MethodInfo Add { get; } = Method(_ => _.Add(default(TKey), default(TValue)));
            public static PropertyInfo Count { get; } = Property(_ => _.Count);
            public static MethodInfo TryGetValue { get; } = Method(_ => _.TryGetValue(default(TKey), out value));
        }

        public class EqualityComparer<T> : Reflections
        {
            public static PropertyInfo Default { get; } = Property(() => System.Collections.Generic.EqualityComparer<T>.Default);
        }

        public class IDisposable : Reflections<System.IDisposable>
        {
            public static MethodInfo Dispose { get; } = Method(_ => _.Dispose());
        }

        public class IEnumerable<T> : Reflections<System.Collections.Generic.IEnumerable<T>>
        {
            public static MethodInfo GetEnumerator { get; } = Method(_ => _.GetEnumerator());
        }

        public class IEnumerator : Reflections<System.Collections.IEnumerator>
        {
            public static MethodInfo MoveNext { get; } = Method(_ => _.MoveNext());
        }

        public class IEnumerator<T> : Reflections<System.Collections.Generic.IEnumerator<T>>
        {
            public static PropertyInfo Current { get; } = Property(_ => _.Current);
        }

        public class IEqualityComparer<T> : Reflections<System.Collections.Generic.IEqualityComparer<T>>
        {
            public static new MethodInfo Equals { get; } = Method(_ => _.Equals(default(T), default(T)));
            public static new MethodInfo GetHashCode { get; } = Method(_ => _.GetHashCode(default(T)));
        }

        public class IEquatable<T> : Reflections<System.IEquatable<T>>
        {
            public static new MethodInfo Equals { get; } = Method(_ => _.Equals(default(T)));
        }

        public class IReadOnlyCollection<T> : Reflections<System.Collections.Generic.IReadOnlyCollection<T>>
        {
            public static PropertyInfo Count { get; } = Property(_ => _.Count);
        }

        public class IReadOnlyDictionary<TKey, TValue> : Reflections<System.Collections.Generic.IReadOnlyDictionary<TKey, TValue>>
        {
            private static TValue value;
            public static MethodInfo TryGetValue { get; } = Method(_ => _.TryGetValue(default(TKey), out value));
        }

        public class KeyValuePair<TKey, TValue> : Reflections<System.Collections.Generic.KeyValuePair<TKey, TValue>>
        {
            public static PropertyInfo Key { get; } = Property(_ => _.Key);
            public static PropertyInfo Value { get; } = Property(_ => _.Value);
        }

        public class List<T> : Reflections<System.Collections.Generic.List<T>>
        {
            public static class New
            {
                public static ConstructorInfo Default { get; } = Constructor(() => new System.Collections.Generic.List<T>());
                public static ConstructorInfo FromIEnumerable { get; } = Constructor(() => new System.Collections.Generic.List<T>(default(System.Collections.Generic.IEnumerable<T>)));
            }

            public static MethodInfo Add { get; } = Method(_ => _.Add(default(T)));
            public static PropertyInfo Items { get; } = Indexer(_ => _[default(Int32)]);
            public static MethodInfo RemoveAt { get; } = Method(_ => _.RemoveAt(default(Int32)));
        }

        public class Object : Reflections<System.Object>
        {
            public static new MethodInfo Equals { get; } = Method(_ => _.Equals(default(Object)));
            public static new MethodInfo GetHashCode { get; } = Method(_ => _.GetHashCode());
            public static new MethodInfo ToString { get; } = Method(_ => _.ToString());
        }

        public class RuntimeHelpers : Reflections
        {
            public static new MethodInfo GetHashCode { get; } = Method(() => System.Runtime.CompilerServices.RuntimeHelpers.GetHashCode(default(Object)));
        }

        public class String : Reflections<System.String>
        {
            public static MethodInfo Concat2 { get; } = Method(() => System.String.Concat(default(System.String), default(System.String)));
        }

        public class Tuple<T1, T2> : Reflections<System.Tuple<T1, T2>>
        {
            public static PropertyInfo Item1 { get; } = Property(_ => _.Item1);
            public static PropertyInfo Item2 { get; } = Property(_ => _.Item2);
        }

        public class NotImplementedException : Reflections<System.NotImplementedException>
        {
            public static class New
            {
                public static ConstructorInfo FromString { get; } = Constructor(() => new System.NotImplementedException(default(System.String)));
            }
        }

        public class NotSupportedException : Reflections<System.NotSupportedException>
        {
            public static class New
            {
                public static ConstructorInfo FromString { get; } = Constructor(() => new System.NotSupportedException(default(System.String)));
            }
        }
    }
}
