using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers
{
    public sealed class TrivialEqualityComparer<T> : IEqualityComparerEx<T>
    {
        public Boolean Equals(T x, T y) =>
            false;

        public Int32 GetHashCode(T obj) =>
            0;

        public Expression EqualsExpression(Expression x, Expression y) =>
            Expression.Constant(false);

        public Expression GetHashCodeExpression(Expression obj) =>
            Expression.Constant(0);
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<T> TrivialEC<T>() =>
            new TrivialEqualityComparer<T>();
    }
}
