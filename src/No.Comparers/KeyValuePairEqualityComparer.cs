using System;
using System.Collections.Generic;

namespace No.Comparers
{
    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<KeyValuePair<TKey, TValue>> KeyValuePairEC<TKey, TValue>(IEqualityComparer<TKey> keyEC, IEqualityComparer<TValue> valueEC) =>
            new ComponentsEqualityComparer<KeyValuePair<TKey, TValue>>
            {
                { _ => _.Key, keyEC },
                { _ => _.Value, valueEC },
            };
    }
}
