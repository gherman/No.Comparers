using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace No.Comparers
{
    public sealed class IdentityEqualityComparer<T> : IEqualityComparerEx<T>
        where T : class
    {
        public Boolean Equals(T x, T y) =>
            ReferenceEquals(x, y);

        public Int32 GetHashCode(T obj) =>
            RuntimeHelpers.GetHashCode(obj);

        public Expression EqualsExpression(Expression x, Expression y) =>
            Expression.ReferenceEqual(x, y);

        public Expression GetHashCodeExpression(Expression obj) =>
            Expression.Call(Reflections.RuntimeHelpers.GetHashCode, obj);
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<T> IdentityEC<T>()
            where T : class =>
            new IdentityEqualityComparer<T>();

        public static IEqualityComparer<T> IdentityECChecked<T>()
        {
            var comp = typeof(EqualityComparerFactories).GetTypeInfo().
                GetDeclaredMethod(nameof(IdentityEC)).
                MakeGenericMethod(typeof(T)).
                Invoke(null, new Object[] { });
            return (IEqualityComparer<T>)comp;
        }
    }
}
