using System;
using System.Collections.Generic;

namespace No.Comparers
{
    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<ValueTuple<T1>> ValueTupleEC<T1>(IEqualityComparer<T1> ec1) =>
            new ComponentsEqualityComparer<ValueTuple<T1>>
            {
                { _ => _.Item1, ec1 },
            };

        public static IEqualityComparer<(T1, T2)> ValueTupleEC<T1, T2>(IEqualityComparer<T1> ec1, IEqualityComparer<T2> ec2) =>
            new ComponentsEqualityComparer<(T1, T2)>
            {
                { _ => _.Item1, ec1 },
                { _ => _.Item2, ec2 },
            };

        public static IEqualityComparer<(T1, T2, T3)> ValueTupleEC<T1, T2, T3>(IEqualityComparer<T1> ec1, IEqualityComparer<T2> ec2, IEqualityComparer<T3> ec3) =>
            new ComponentsEqualityComparer<(T1, T2, T3)>
            {
                { _ => _.Item1, ec1 },
                { _ => _.Item2, ec2 },
                { _ => _.Item3, ec3 },
            };

        public static IEqualityComparer<(T1, T2, T3, T4)> ValueTupleEC<T1, T2, T3, T4>(IEqualityComparer<T1> ec1, IEqualityComparer<T2> ec2, IEqualityComparer<T3> ec3, IEqualityComparer<T4> ec4) =>
            new ComponentsEqualityComparer<(T1, T2, T3, T4)>
            {
                { _ => _.Item1, ec1 },
                { _ => _.Item2, ec2 },
                { _ => _.Item3, ec3 },
                { _ => _.Item4, ec4 },
            };

        public static IEqualityComparer<(T1, T2, T3, T4, T5)> ValueTupleEC<T1, T2, T3, T4, T5>(IEqualityComparer<T1> ec1, IEqualityComparer<T2> ec2, IEqualityComparer<T3> ec3, IEqualityComparer<T4> ec4, IEqualityComparer<T5> ec5) =>
            new ComponentsEqualityComparer<(T1, T2, T3, T4, T5)>
            {
                { _ => _.Item1, ec1 },
                { _ => _.Item2, ec2 },
                { _ => _.Item3, ec3 },
                { _ => _.Item4, ec4 },
                { _ => _.Item5, ec5 },
            };

        public static IEqualityComparer<(T1, T2, T3, T4, T5, T6)> ValueTupleEC<T1, T2, T3, T4, T5, T6>(IEqualityComparer<T1> ec1, IEqualityComparer<T2> ec2, IEqualityComparer<T3> ec3, IEqualityComparer<T4> ec4, IEqualityComparer<T5> ec5, IEqualityComparer<T6> ec6) =>
            new ComponentsEqualityComparer<(T1, T2, T3, T4, T5, T6)>
            {
                { _ => _.Item1, ec1 },
                { _ => _.Item2, ec2 },
                { _ => _.Item3, ec3 },
                { _ => _.Item4, ec4 },
                { _ => _.Item5, ec5 },
                { _ => _.Item6, ec6 },
            };

        public static IEqualityComparer<(T1, T2, T3, T4, T5, T6, T7)> ValueTupleEC<T1, T2, T3, T4, T5, T6, T7>(IEqualityComparer<T1> ec1, IEqualityComparer<T2> ec2, IEqualityComparer<T3> ec3, IEqualityComparer<T4> ec4, IEqualityComparer<T5> ec5, IEqualityComparer<T6> ec6, IEqualityComparer<T7> ec7) =>
            new ComponentsEqualityComparer<(T1, T2, T3, T4, T5, T6, T7)>
            {
                { _ => _.Item1, ec1 },
                { _ => _.Item2, ec2 },
                { _ => _.Item3, ec3 },
                { _ => _.Item4, ec4 },
                { _ => _.Item5, ec5 },
                { _ => _.Item6, ec6 },
                { _ => _.Item7, ec7 },
            };

        public static IEqualityComparer<(T1, T2, T3, T4, T5, T6, T7, T8)> ValueTupleEC<T1, T2, T3, T4, T5, T6, T7, T8>(IEqualityComparer<T1> ec1, IEqualityComparer<T2> ec2, IEqualityComparer<T3> ec3, IEqualityComparer<T4> ec4, IEqualityComparer<T5> ec5, IEqualityComparer<T6> ec6, IEqualityComparer<T7> ec7, IEqualityComparer<T8> ec8) =>
            new ComponentsEqualityComparer<(T1, T2, T3, T4, T5, T6, T7, T8)>
            {
                { _ => _.Item1, ec1 },
                { _ => _.Item2, ec2 },
                { _ => _.Item3, ec3 },
                { _ => _.Item4, ec4 },
                { _ => _.Item5, ec5 },
                { _ => _.Item6, ec6 },
                { _ => _.Item7, ec7 },
                { _ => _.Item8, ec8 },
            };
    }
}
