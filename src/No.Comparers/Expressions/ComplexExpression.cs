using System;
using System.Linq.Expressions;

namespace No.Comparers.Expressions
{
    public static class ComplexExpression
    {
        public static Expression Using(Expression value, Func<ParameterExpression, Expression> makeBody)
        {
            var variable = Expression.Variable(value.Type);
            var body = makeBody(variable);
            return Expression.Block(
                body.Type,
                new[] { variable },
                Expression.Assign(variable, value),
                Expression.TryFinally(
                    body,
                    Expression.Call(variable, Reflections.IDisposable.Dispose)));
        }

        public static Expression Using(Expression value1, Expression value2, Func<ParameterExpression, ParameterExpression, Expression> makeBody)
        {
            var variable1 = Expression.Variable(value1.Type);
            var variable2 = Expression.Variable(value2.Type);
            var body = makeBody(variable1, variable2);
            return Expression.Block(
                body.Type,
                new[] { variable1, variable2 },
                Expression.Assign(variable1, value1),
                Expression.Assign(variable2, value2),
                Expression.TryFinally(
                    body,
                    Expression.Block(
                        Expression.Call(variable1, Reflections.IDisposable.Dispose),
                        Expression.Call(variable2, Reflections.IDisposable.Dispose))));
        }

        public static Expression While(Expression condition, Func<LabelTarget, Expression> makeBody)
        {
            var @break = Expression.Label();
            return Expression.Loop(
                Expression.IfThenElse(
                    condition,
                    makeBody(@break),
                    Expression.Break(@break)),
                @break);
        }

        public static Expression While(Expression condition, Expression body)
        {
            return While(condition, _ => body);
        }

        public static Expression ForEach<T>(Expression enumerable, Func<ParameterExpression, LabelTarget, Expression> makeBody)
        {
            var item = Expression.Variable(typeof(T));
            return Using(
                Expression.Call(enumerable, Reflections.IEnumerable<T>.GetEnumerator),
                enumerator => While(
                    Expression.Call(enumerator, Reflections.IEnumerator.MoveNext),
                    @break => Expression.Block(
                        new[] { item },
                        Expression.Assign(
                            item,
                            Expression.Property(enumerator, Reflections.IEnumerator<T>.Current)),
                        makeBody(item, @break))));
        }

        public static Expression ForEach<T>(Expression enumerable, Func<ParameterExpression, Expression> makeBody)
        {
            return ForEach<T>(enumerable, (item, _) => makeBody(item));
        }
    }
}
