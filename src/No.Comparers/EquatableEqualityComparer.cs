using System;
using System.Linq.Expressions;
using System.Reflection;

namespace No.Comparers
{
    using System.Collections.Generic;
    using static Expression;

    public sealed class EquatableEqualityComparer<T> : IEqualityComparerEx<T>
        where T : IEquatable<T>
    {
        public Boolean Equals(T x, T y) =>
            x.Equals(y);

        public Int32 GetHashCode(T obj) =>
            obj.GetHashCode();

        public Expression EqualsExpression(Expression x, Expression y) =>
            Call(x, Reflections.IEquatable<T>.Equals, y);

        public Expression GetHashCodeExpression(Expression obj) =>
            Call(obj, Reflections.Object.GetHashCode);
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<T> EquatableEC<T>()
            where T : IEquatable<T>
        {
            return new EquatableEqualityComparer<T>();
        }

        public static IEqualityComparer<T> EquatableECChecked<T>()
        {
            var comp = typeof(EqualityComparerFactories).GetTypeInfo().
                GetDeclaredMethod(nameof(EquatableEC)).
                MakeGenericMethod(typeof(T)).
                Invoke(null, new Object[] { });
            return (IEqualityComparer<T>)comp;
        }
    }
}
