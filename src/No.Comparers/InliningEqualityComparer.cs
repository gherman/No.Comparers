using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers
{
    using static Expression;

    public abstract class InliningEqualityComparer<T>
        : IEqualityComparerEx<T>
    {
        private static readonly ParameterExpression X;
        private static readonly ParameterExpression Y;
        private static readonly ParameterExpression Obj;

        static InliningEqualityComparer()
        {
            X = Parameter(typeof(T), nameof(X));
            Y = Parameter(typeof(T), nameof(Y));
            Obj = Parameter(typeof(T), nameof(Obj));
        }

        public abstract Expression EqualsExpression(Expression x, Expression y);
        public abstract Expression GetHashCodeExpression(Expression obj);

        private new Func<T, T, Boolean> Equals;
        private new Func<T, Int32> GetHashCode;

        protected InliningEqualityComparer()
        {
            Equals = (x, y) =>
            {
                var body = EqualsExpression(X, Y);
                var lambda = Lambda<Func<T, T, Boolean>>(body, X, Y);
                Equals = lambda.Compile();
                return Equals(x, y);
            };
            GetHashCode = (obj) =>
            {
                var body = GetHashCodeExpression(Obj);
                var lambda = Lambda<Func<T, Int32>>(body, Obj);
                GetHashCode = lambda.Compile();
                return GetHashCode(obj);
            };
        }

        Boolean IEqualityComparer<T>.Equals(T x, T y) =>
            Equals(x, y);

        Int32 IEqualityComparer<T>.GetHashCode(T obj) =>
            GetHashCode(obj);
    }
}
