using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers
{
    using System.Reflection;
    using static Expression;

    public sealed class NullEqualityComparer<T> : InliningEqualityComparer<T>
        where T : class
    {
        private readonly IEqualityComparer<T> InnerEC;

        public NullEqualityComparer(IEqualityComparer<T> innerEC)
        {
            if (innerEC == null)
                throw new ArgumentNullException(nameof(innerEC));
            InnerEC = innerEC;
        }

        public override Expression EqualsExpression(Expression x, Expression y)
        {
            var copyX = Variable(typeof(T));
            var copyY = Variable(typeof(T));
            return Block(
                typeof(Boolean),
                new[] { copyX, copyY },
                Assign(
                    copyX,
                    x),
                Assign(
                    copyY,
                    y),
                Condition(
                    ReferenceEqual(
                        copyX,
                        Constant(null)),
                    ReferenceEqual(
                        copyY,
                        Constant(null)),
                    Condition(
                        ReferenceEqual(
                            copyY,
                            Constant(null)),
                        Constant(false),
                        InnerEC.EqualsExpression(
                            copyX,
                            copyY))));
        }

        public override Expression GetHashCodeExpression(Expression obj)
        {
            var copy = Variable(typeof(T));
            return Block(
                typeof(Int32),
                new[] { copy },
                Assign(
                    copy,
                    obj),
                Condition(
                    ReferenceEqual(
                        copy,
                        Constant(null)),
                    Constant(0),
                    InnerEC.GetHashCodeExpression(
                        copy)));
        }
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<T> NullEC<T>(IEqualityComparer<T> innerEC)
            where T : class =>
            new NullEqualityComparer<T>(innerEC);

        public static IEqualityComparer<T> NullECChecked<T>(IEqualityComparer<T> innerEC)
        {
            var comp = typeof(EqualityComparerFactories).GetTypeInfo().
                GetDeclaredMethod(nameof(NullEC)).
                MakeGenericMethod(typeof(T)).
                Invoke(null, new Object[] { innerEC });
            return (IEqualityComparer<T>)comp;
        }
    }
}
