using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers.Expressions
{
    public sealed class MemoizingVisitor : ExpressionVisitor
    {
        private sealed class LocalComparers : ExpressionEqualityComparers
        {
            public override IEqualityComparer<Expression> SubexpressionEC { get; } =
                IdentityEC;
        }

        private static readonly ExpressionEqualityComparers Comparers =
            new LocalComparers();

        private readonly Dictionary<Expression, Expression> Memory =
            new Dictionary<Expression, Expression>(Comparers.ExpressionEC);

        public override Expression Visit(Expression node) =>
            (node == null) ? null : Memory.GetOrAdd(node, base.Visit);
    }
}
