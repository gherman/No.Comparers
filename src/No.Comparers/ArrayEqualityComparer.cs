using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers
{
    using static Expression;
    using static Expressions.ComplexExpression;

    public class ArrayEqualityComparer<T> : InliningEqualityComparer<T[]>
    {
        private readonly IEqualityComparer<T> ItemEC;

        public ArrayEqualityComparer(IEqualityComparer<T> itemEC)
        {
            if (itemEC == null)
                throw new ArgumentNullException(nameof(itemEC));
            ItemEC = itemEC;
        }

        public override Expression EqualsExpression(Expression x, Expression y)
        {
            var tempx = Variable(x.Type);
            var tempy = Variable(y.Type);
            var index = Variable(typeof(Int32));
            var @return = Label(typeof(Boolean));
            return Block(
                typeof(Boolean),
                new [] { tempx, tempy, index },
                Assign(
                    tempx,
                    x),
                Assign(
                    tempy,
                    y),
                IfThen(
                    NotEqual(
                        ArrayLength(
                            tempx),
                        ArrayLength(
                            tempy)),
                    Goto(
                        @return,
                        Constant(false))),
                Assign(
                    index,
                    Constant(0)),
                While(
                    LessThan(
                        index,
                        ArrayLength(
                            tempx)),
                    Block(
                        IfThen(
                            Not(
                                ItemEC.EqualsExpression(
                                    ArrayIndex(
                                        tempx,
                                        index),
                                    ArrayIndex(
                                        tempy,
                                        index))),
                            Goto(
                                @return,
                                Constant(false))),
                        PostIncrementAssign(
                            index))),
                Label(
                    @return,
                    Constant(true))
            );
        }

        public override Expression GetHashCodeExpression(Expression obj)
        {
            var temp = Variable(obj.Type);
            var index = Variable(typeof(Int32));
            var result = Variable(typeof(Int32));
            return Block(
                typeof(Int32),
                new [] { temp, index, result },
                Assign(
                    temp,
                    obj),
                Assign(
                    result,
                    Constant(MagicNumbers<IEnumerable<T>>.Get(0))),
                Assign(
                    index,
                    Constant(0)),
                While(
                    LessThan(
                        index,
                        ArrayLength(
                            temp)),
                    Block(
                        Assign(
                            result,
                            Add(
                                Multiply(
                                    Constant(MagicNumbers<IEnumerable<T>>.Get(1)),
                                    result),
                                ExclusiveOr(
                                    Constant(MagicNumbers<IEnumerable<T>>.Get(2)),
                                    ItemEC.GetHashCodeExpression(
                                        ArrayIndex(
                                            temp,
                                            index))))),
                        PostIncrementAssign(
                            index))),
                result);
        }
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<T[]> ArrayEC<T>(IEqualityComparer<T> itemEC) =>
            new ArrayEqualityComparer<T>(itemEC);
    }
}
