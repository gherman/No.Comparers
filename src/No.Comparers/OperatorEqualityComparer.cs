using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers
{
    public sealed class OperatorEqualityComparer<T> : InliningEqualityComparer<T>
    {
        public override Expression EqualsExpression(Expression x, Expression y) =>
            Expression.Equal(x, y);

        public override Expression GetHashCodeExpression(Expression obj) =>
            Expression.Call(obj, Reflections.Object.GetHashCode);
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<T> OperatorEC<T>() =>
            new OperatorEqualityComparer<T>();
    }
}
