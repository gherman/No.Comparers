using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace No.Comparers
{
    using Expressions;
    using static Expression;

    public class VariantsEqualityComparer<T, TTag> : InliningEqualityComparer<T>, IEnumerable
    {
        private readonly Expression<Func<T, TTag>> TagSelector;
        private readonly IEqualityComparer<TTag> TagEC;
        private Boolean Frozen;
        private Action<IProcessor> ProcessVariants =
            _ => { };

        public VariantsEqualityComparer(Expression<Func<T, TTag>> tagSelector, IEqualityComparer<TTag> tagEC)
        {
            if (tagSelector == null)
                throw new ArgumentNullException(nameof(tagSelector));
            if (tagEC == null)
                throw new ArgumentNullException(nameof(tagEC));
            TagSelector = tagSelector;
            TagEC = tagEC;
        }

        public void Add<TVariant>(TTag tag, Expression<Func<T, TVariant>> selector, IEqualityComparer<TVariant> comparer)
        {
            if (Frozen)
                throw new InvalidOperationException($"Cannot add components to a {nameof(VariantsEqualityComparer<T, TTag>)} after it has been used.");
            if (selector == null)
                throw new ArgumentNullException(nameof(selector));
            if (comparer == null)
                throw new ArgumentNullException(nameof(comparer));
            ProcessVariants += _ => _.Process(tag, selector, comparer);
        }

        public interface IProcessor
        {
            void Process<TVariant>(TTag tag, Expression<Func<T, TVariant>> selector, IEqualityComparer<TVariant> comparer);
        }

        private abstract class Builder
        {
            protected VariantsEqualityComparer<T, TTag> Owner { get; }

            protected Builder(VariantsEqualityComparer<T, TTag> owner)
            {
                Owner = owner;
            }

            protected Expression Error<TResult>(Expression tag) =>
                Block(
                    Throw(
                        New(
                            Reflections.NotImplementedException.New.FromString,
                            Call(
                                Reflections.String.Concat2,
                                Constant($"{nameof(VariantsEqualityComparer<T, TTag>)} cannot handle tag "),
                                Call(
                                    tag,
                                    Reflections.Object.ToString)))),
                    Default(typeof(TResult)));
        }

        private sealed class EqualsBuilder : Builder, IProcessor
        {
            private readonly ParameterExpression X;
            private readonly ParameterExpression TagX;
            private readonly ParameterExpression Y;
            private readonly ParameterExpression TagY;
            private readonly LabelTarget Return;
            private readonly List<Expression> Prefix;
            private readonly List<Expression> Cases;
            private readonly List<Expression> Suffix;

            public EqualsBuilder(VariantsEqualityComparer<T, TTag> owner, Expression x, Expression y)
                : base(owner)
            {
                X = Parameter(typeof(T));
                TagX = Parameter(typeof(TTag));
                Y = Parameter(typeof(T));
                TagY = Parameter(typeof(TTag));
                Return = Label(typeof(Boolean));
                Prefix = new List<Expression>
                {
                    Assign(X, x),
                    Assign(TagX, Owner.TagSelector.SubstituteParametersWith(X)),
                    Assign(Y, y),
                    Assign(TagY, Owner.TagSelector.SubstituteParametersWith(Y)),
                    IfThen(
                        Not(
                            Owner.TagEC.EqualsExpression(
                                TagX,
                                TagY)),
                        Goto(
                            Return,
                            Constant(false))),
                };
                Cases = new List<Expression>();
                Suffix = new List<Expression>
                {
                    Label(Return, Error<Boolean>(TagX)),
                };
            }

            public void Process<TVariant>(TTag tag, Expression<Func<T, TVariant>> selector, IEqualityComparer<TVariant> comparer) =>
                Cases.Add(
                    IfThen(
                        Owner.TagEC.EqualsExpression(
                            TagX,
                            Constant(tag)),
                        Goto(
                            Return,
                            comparer.EqualsExpression(
                                selector.SubstituteParametersWith(X),
                                selector.SubstituteParametersWith(Y)))));

            public Expression Result =>
                Block(typeof(Boolean), new[] { X, TagX, Y, TagY }, Prefix.Union(Cases).Union(Suffix));
        }

        public override Expression EqualsExpression(Expression x, Expression y)
        {
            Frozen = true;
            var builder = new EqualsBuilder(this, x, y);
            ProcessVariants(builder);
            return builder.Result;
        }

        private sealed class GetHashCodeBuilder : Builder, IProcessor
        {
            private readonly ParameterExpression Obj;
            private readonly ParameterExpression Tag;
            private readonly LabelTarget Return;
            private readonly List<Expression> Prefix;
            private readonly List<Expression> Cases;
            private readonly List<Expression> Suffix;
            private Int32 Index;

            public GetHashCodeBuilder(VariantsEqualityComparer<T, TTag> owner, Expression obj)
                : base(owner)
            {
                Obj = Parameter(typeof(T));
                Tag = Parameter(typeof(TTag));
                Return = Label(typeof(Int32));
                Prefix = new List<Expression>
                {
                    Assign(Obj, obj),
                    Assign(Tag, Owner.TagSelector.SubstituteParametersWith(Obj)),
                };
                Cases = new List<Expression>();
                Suffix = new List<Expression>
                {
                    Label(Return, Error<Int32>(Tag)),
                };
                Index = 0;
            }

            public void Process<TVariant>(TTag tag, Expression<Func<T, TVariant>> selector, IEqualityComparer<TVariant> comparer) =>
                Cases.Add(
                    IfThen(
                        Owner.TagEC.EqualsExpression(
                            Tag,
                            Constant(tag)),
                        Goto(
                            Return,
                            ExclusiveOr(
                                Constant(MagicNumbers<T>.Get(Index++)),
                                comparer.GetHashCodeExpression(
                                    selector.SubstituteParametersWith(Obj))))));

            public Expression Result =>
                Block(typeof(Int32), new[] { Obj, Tag }, Prefix.Union(Cases).Union(Suffix));
        }

        public override Expression GetHashCodeExpression(Expression obj)
        {
            Frozen = true;
            var builder = new GetHashCodeBuilder(this, obj);
            ProcessVariants(builder);
            return builder.Result;
        }

        IEnumerator IEnumerable.GetEnumerator() =>
            throw new InvalidOperationException($"{nameof(ComponentsEqualityComparer<T>)} is not enumerable. It implements IEnumerable only to support collection initialization syntax.");
    }

    public class VariantsEqualityComparer<T> : VariantsEqualityComparer<T, Type>
    {
        public VariantsEqualityComparer(Expression<Func<T, Type>> tagSelector)
            : base(tagSelector, EqualityComparer<Type>.Default)
        {
        }

        public void Add<TVariant>(IEqualityComparer<TVariant> variantEC)
            where TVariant : T =>
            base.Add(typeof(TVariant), _ => (TVariant)_, variantEC);
    }
}
