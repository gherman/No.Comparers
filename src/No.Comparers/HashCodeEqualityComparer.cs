using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers
{
    using static Expression;

    public class HashCodeEqualityComparer<T> : InliningEqualityComparer<T>
    {
        private readonly IEqualityComparer<T> InnerEC;

        public HashCodeEqualityComparer(IEqualityComparer<T> innerEC)
        {
            if (innerEC == null)
                throw new ArgumentNullException(nameof(innerEC));
            InnerEC = innerEC;
        }

        public override Expression EqualsExpression(Expression x, Expression y)
        {
            var tempx = Variable(x.Type);
            var tempy = Variable(y.Type);
            return Block(
                typeof(Boolean),
                new[] { tempx, tempy },
                Assign(
                    tempx,
                    x),
                Assign(
                    tempy,
                    y),
                AndAlso(
                    Equal(
                        InnerEC.GetHashCodeExpression(tempx),
                        InnerEC.GetHashCodeExpression(tempy)),
                    InnerEC.EqualsExpression(tempx, tempy)));
        }

        public override Expression GetHashCodeExpression(Expression obj) =>
            InnerEC.GetHashCodeExpression(obj);
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<T> HashCodeEC<T>(IEqualityComparer<T> innerEC) =>
            new HashCodeEqualityComparer<T>(innerEC);
    }
}
