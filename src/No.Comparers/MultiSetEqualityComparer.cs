using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers
{
    using static Expression;
    using static Expressions.ComplexExpression;

    public class MultiSetEqualityComparer<T> : InliningEqualityComparer<IEnumerable<T>>
    {
        private readonly IEqualityComparer<T> ItemEC;

        public MultiSetEqualityComparer(IEqualityComparer<T> itemEC)
        {
            if (itemEC == null)
                throw new ArgumentNullException(nameof(itemEC));
            ItemEC = itemEC;
        }

        public override Expression EqualsExpression(Expression x, Expression y)
        {
            var xcopy = Variable(typeof(IEnumerable<T>));
            var xitems = Variable(typeof(IReadOnlyCollection<T>));
            var yitems = Variable(typeof(IEnumerable<T>));
            var size = Variable(typeof(UInt32));
            var buckets = Variable(typeof(List<T>[]));
            var xhash = Variable(typeof(Int32));
            var xbucket = Variable(typeof(List<T>));
            var found = Variable(typeof(Int32));
            var yhash = Variable(typeof(Int32));
            var ybucket = Variable(typeof(List<T>));
            var yindex = Variable(typeof(Int32));
            var @return = Label(typeof(Boolean));
            return Block(
                typeof(Boolean),
                new[] { xcopy, xitems, size, buckets, found, yitems },
                Assign(
                    xcopy,
                    x),
                Assign(
                    xitems,
                    Coalesce(
                        TypeAs(
                            xcopy,
                            typeof(IReadOnlyCollection<T>)),
                        New(
                            Reflections.List<T>.New.FromIEnumerable,
                            xcopy))),
                Assign(
                    size,
                    Convert(
                        Increment(
                            MultiplyChecked(
                                Constant(3),
                                Property(
                                    xitems,
                                    Reflections.IReadOnlyCollection<T>.Count))),
                        typeof(UInt32))),
                Assign(
                    buckets,
                    NewArrayBounds(
                        typeof(List<T>),
                        size)),
                ForEach<T>(xitems, item =>
                    Block(
                        new[] { xhash, xbucket },
                        Assign(
                            xhash,
                            Convert(
                                Modulo(
                                    Convert(
                                        ItemEC.GetHashCodeExpression(item),
                                        typeof(UInt32)),
                                    size),
                                typeof(Int32))),
                        Assign(
                            xbucket,
                            ArrayAccess(
                                buckets,
                                xhash)),
                        IfThen(
                            ReferenceEqual(
                                xbucket,
                                Constant(null)),
                            Assign(
                                ArrayAccess(
                                    buckets,
                                    xhash),
                                Assign(
                                    xbucket,
                                    New(
                                        Reflections.List<T>.New.Default)))),
                        Call(
                            xbucket,
                            Reflections.List<T>.Add,
                            item))),
                Assign(
                    found,
                    Constant(0)),
                Assign(
                    yitems,
                    y),
                ForEach<T>(yitems, item =>
                    Block(
                        new[] { yhash, ybucket, yindex },
                        Assign(
                            yhash,
                            Convert(
                                Modulo(
                                    Convert(
                                        ItemEC.GetHashCodeExpression(item),
                                        typeof(UInt32)),
                                    size),
                                typeof(Int32))),
                        Assign(
                            ybucket,
                            ArrayAccess(
                                buckets,
                                yhash)),
                        IfThen(
                            ReferenceEqual(
                                ybucket,
                                Constant(null)),
                            Break(
                                @return,
                                Constant(false))),
                        Assign(
                            yindex,
                            Decrement(
                                Property(
                                    ybucket,
                                    Reflections.IReadOnlyCollection<T>.Count))),
                        While(
                            AndAlso(
                                GreaterThanOrEqual(
                                    yindex,
                                    Constant(0)),
                                Not(
                                    ItemEC.EqualsExpression(
                                        MakeIndex(
                                            ybucket,
                                            Reflections.List<T>.Items,
                                            new[] { yindex }),
                                        item))),
                            PreDecrementAssign(
                                yindex)),
                        IfThen(
                            LessThan(
                                yindex,
                                Constant(0)),
                            Break(
                                @return,
                                Constant(false))),
                        Call(
                            ybucket,
                            Reflections.List<T>.RemoveAt,
                            yindex),
                        PreIncrementAssign(
                            found))),
                Label(
                    @return,
                    Equal(
                        found,
                        Property(
                            xitems,
                            Reflections.IReadOnlyCollection<T>.Count))));
        }

        public override Expression GetHashCodeExpression(Expression obj)
        {
            var result = Variable(typeof(Int32));
            return Block(
                typeof(Int32),
                new[] { result },
                Assign(
                    result,
                    Constant(MagicNumbers<IEnumerable<T>>.Get(0))),
                ForEach<T>(obj, (item, _) =>
                    AddAssign(
                        result,
                        Multiply(
                            Add(
                                Multiply(
                                    Constant(2),
                                    result),
                                Constant(1)),
                            ExclusiveOr(
                                Constant(MagicNumbers<IEnumerable<T>>.Get(1)),
                                ItemEC.GetHashCodeExpression(item))))),
                result);
        }
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<IEnumerable<T>> MultiSetEC<T>(IEqualityComparer<T> itemEC) =>
            new MultiSetEqualityComparer<T>(itemEC);
    }
}
