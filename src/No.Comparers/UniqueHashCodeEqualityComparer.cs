using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace No.Comparers
{
    using static Expression;

    public class UniqueHashCodeEqualityComparer<T> : InliningEqualityComparer<T>
    {
        private readonly IEqualityComparer<T> InnerEC;
        private readonly Dictionary<T, Int32> Memory;

        public UniqueHashCodeEqualityComparer(IEqualityComparer<T> innerEC)
        {
            InnerEC = innerEC;
            Memory = new Dictionary<T, Int32>(InnerEC);
        }

        public override Expression EqualsExpression(Expression x, Expression y) =>
            InnerEC.EqualsExpression(x, y);

        public override Expression GetHashCodeExpression(Expression obj)
        {
            var tempobj = Variable(obj.Type);
            var hashCode = Variable(typeof(Int32));
            return Block(
                typeof(Int32),
                new [] { tempobj, hashCode },
                Assign(
                    tempobj,
                    obj),
                IfThen(
                    Not(
                        Call(
                            Constant(Memory),
                            Reflections.Dictionary<T, Int32>.TryGetValue,
                            tempobj,
                            hashCode)),
                    Block(
                        Assign(
                            hashCode,
                            Property(
                                Constant(Memory),
                                Reflections.Dictionary<T, Int32>.Count)),
                        Call(
                            Constant(Memory),
                            Reflections.Dictionary<T, Int32>.Add,
                            tempobj,
                            hashCode))),
                hashCode);
        }
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<T> UniqueHashCodeEC<T>(IEqualityComparer<T> innerEC) =>
            new UniqueHashCodeEqualityComparer<T>(innerEC);
    }
}
