using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace No.Comparers
{
    using static Expression;

    public class MemoizingEqualityComparer<T> : InliningEqualityComparer<T>
        where T : class
    {
        private readonly IEqualityComparer<T> InnerEC;
        private readonly ConditionalWeakTable<T, Object> Memory;

        public MemoizingEqualityComparer(IEqualityComparer<T> innerEC)
        {
            if (innerEC == null)
                throw new ArgumentNullException(nameof(innerEC));
            InnerEC = innerEC;
            Memory = new ConditionalWeakTable<T, Object>();
        }

        public override Expression EqualsExpression(Expression x, Expression y) =>
            InnerEC.EqualsExpression(x, y);

        public override Expression GetHashCodeExpression(Expression obj)
        {
            var tempobj = Variable(obj.Type);
            var hashCode = Variable(typeof(Object));
            return Block(
                typeof(Int32),
                new[] { tempobj, hashCode },
                Assign(
                    tempobj,
                    obj),
                IfThen(
                    Not(
                        Call(
                            Constant(Memory),
                            Reflections.ConditionalWeakTable<T, Object>.TryGetValue,
                            tempobj,
                            hashCode)),
                    Block(
                        Assign(
                            hashCode,
                            Convert(
                                InnerEC.GetHashCodeExpression(tempobj),
                                typeof(Object))),
                        Call(
                            Constant(Memory),
                            Reflections.ConditionalWeakTable<T, Object>.Add,
                            tempobj,
                            hashCode))),
                ConvertChecked(
                    hashCode,
                    typeof(Int32)));
        }
    }

    public static partial class EqualityComparerFactories
    {
        public static IEqualityComparer<T> MemoizingEC<T>(IEqualityComparer<T> innerEC)
            where T : class =>
            new MemoizingEqualityComparer<T>(innerEC);

        public static IEqualityComparer<T> MemoizingECChecked<T>(IEqualityComparer<T> innerEC)
        {
            var comp = typeof(EqualityComparerFactories).GetTypeInfo().
                GetDeclaredMethod(nameof(MemoizingEC)).
                MakeGenericMethod(typeof(T)).
                Invoke(null, new Object[] { innerEC });
            return (IEqualityComparer<T>)comp;
        }
    }
}
